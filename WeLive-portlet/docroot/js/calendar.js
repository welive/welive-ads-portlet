    var monthNames= ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday", "Sunday"];
    var daysSort = ["M","T","W","T","F","S","S"];
    function createCalendar(idtag, year){
        var cal=document.getElementById(idtag);
        cal.innerHTML='';
        var strweek='';
        for (i=0;i<7;i++){
            strweek+='<div class="dayTitle">'+daysSort[i]+'</div>'
        }
        for (i=0;i<12;i++){
            cal.innerHTML+='<div class="month">'+
                    '<div class="monTitle">'+monthNames[i]+'</div>'+
                    '<div class="monBody" id="mon'+i+'">'+strweek+'</div>'+
                    '</div>';
        }
        
        createDays(year);
    
    }
    
    function createMonth(idtag, idinput, year, month){
        var cal=document.getElementById(idtag);
        cal.innerHTML='';
        var strweek='';
        for (i=0;i<7;i++){
            strweek+='<div class="dayTitle">'+daysSort[i]+'</div>'
        }
        
        var yearb=year;
        var monthb=month-1;
        var yeara=year;
        var montha=month+1;
        
        if (month==11){
            monthb=10;
            yeara=year+1;
            montha=0;
        }
        if (month==0){
            monthb=11;
            yearb=year-1;
            montha=1;
        }
        
		cal.innerHTML+='<div class="month picker">'+
		        '<div class="monTitle">'+
		        '<div class="monthBefore"  onclick="createMonth(\''+idtag+'\',\''+idinput+'\','+yearb+','+monthb+')"><</div>'+
		        monthNames[month]+' '+year+
		        '<div class="monthAfter"  onclick="hidePicker(\''+idtag+'\')">X</div>'+
		        '<div class="monthAfter"  onclick="createMonth(\''+idtag+'\',\''+idinput+'\','+yeara+','+montha+')">></div>'+
		        '</div>'+
		        '<div class="monBody" id="month'+idtag+month+'">'+strweek+'</div>'+
		        '</div>';
        
        createDaysMonth(year, month, idtag, idinput);
    
    }

    function createDays(year){
        var startDate= new Date(year, 0, 1);
        var endDate= new Date(year+1, 0, 1);
        var i=0;
        for (currday=startDate; currday<endDate; currday.setDate(currday.getDate() + 1)){
            i++;
            var month=document.getElementById('mon'+currday.getMonth());
            if (currday.getDate()==1){
                for (j=0;j<currday.getDay()-1;j++){
                    month.innerHTML+='<div class="dayWeek"></div>';
                }
            }
            
            month.innerHTML+='<div class="dayWeek" onclick="alert(\''+currday+'\')"  id="day'+i+'">'+currday.getDate()+'</div>';
            
        }
    
    }

    
    function createDaysMonth(year, month, idtag, idinput){
    	
        var startDate= new Date(year, month, 1);
        if (month==11){
        	month=0;
        	year++;
        }else{
        	month++;
        }
        var endDate= new Date(year, month, 1);
        var i=0;
        for (currday=startDate; currday<endDate; currday.setDate(currday.getDate() + 1)){
            i++;
            var month=document.getElementById('month'+idtag+currday.getMonth());
            if (currday.getDate()==1){
                for (j=0;j<currday.getDay()-1;j++){
                    month.innerHTML+='<div class="dayWeek"></div>';
                }
            }
            
            month.innerHTML+='<div class="dayWeek" onclick="pickDay('+currday.getFullYear()+','+(currday.getMonth()+1)+', '+currday.getDate()+', \''+idtag+'\', \''+idinput+'\' )"  id="day'+i+'">'+currday.getDate()+'</div>';
            
        }
    
    }  
    function pickDay(y, m, d, idtag, idinput){
    	d=("0"+d).substr(-2);
    	m=("0"+m).substr(-2);
    	
    	document.getElementById(idinput).value=y+"-"+m+"-"+d;
    	document.getElementById(idtag).style.display="none";
    	document.getElementById(idtag).innerHTML="";
    }
    
    function showPicker(idtag, obj){
    	document.getElementById(idtag).innerHTML="";
    	var d = new Date();
    	createMonth(idtag, obj.id, d.getFullYear(), d.getMonth());
    	document.getElementById(idtag).style.display="block";
    }
    
    function hidePicker(idtag){
    	document.getElementById(idtag).innerHTML="";
    	document.getElementById(idtag).style.display="none";
    }    