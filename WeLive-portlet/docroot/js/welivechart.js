	var colores=["105, 179, 231","239, 51, 64", "181, 189,0", "151,187,205", "220,220,220", "75,192,192", "255,99,132", "220,220,220","151,187,205", "75,192,192", "255,99,132" , "220,220,220","151,187,205", "75,192,192", "255,99,132"];
	var currentCharts= [];
	
	var optionsBar= {
		    scaleBeginAtZero : true,
		    scaleShowGridLines : true,
		    scaleGridLineColor : "rgba(0,0,0,.05)",
		    scaleGridLineWidth : 1,
		    scaleShowHorizontalLines: true,
		    scaleShowVerticalLines: true,
		    barShowStroke : true,
		    barStrokeWidth : 2,
		    barValueSpacing : 5,
		    labelLength:7,
		    barDatasetSpacing : 1,
		    multiTooltipTemplate: "<%=datasetLabel %>: <%= value %>",
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
	};
	var optionsLine={
	    scaleShowGridLines : true,
	    scaleGridLineColor : "rgba(0,0,0,.05)",
	    scaleGridLineWidth : 1,
	    scaleShowHorizontalLines: true,
	    scaleShowVerticalLines: true,
	    bezierCurve : false,
	    bezierCurveTension : 0.4,
	    pointDot : true,
	    pointDotRadius : 2,
	    pointDotStrokeWidth : 1,
	    pointHitDetectionRadius : 20,
	    labelLength:8,
	    datasetStroke : true,
	    datasetStrokeWidth : 2,
	    datasetFill : true,
	    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
	};

	var optionsRadar= {
		    scaleShowLine : true,
		    angleShowLineOut : true,
		    scaleShowLabels : false,
		    scaleBeginAtZero : true,
		    angleLineColor : "rgba(0,0,0,.1)",
		    angleLineWidth : 1,
		    pointLabelFontFamily : "'Arial'",
		    pointLabelFontStyle : "normal",
		    pointLabelFontSize : 10,
		    pointLabelFontColor : "#666",
		    pointDot : true,
		    pointDotRadius : 3,
		    pointDotStrokeWidth : 1,
		    pointHitDetectionRadius : 20,
		    datasetStroke : true,
		    datasetStrokeWidth : 2,
		    datasetFill : true,
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
		}
	var optionsPie={
		    segmentShowStroke : true,
		    segmentStrokeColor : "#fff",
		    segmentStrokeWidth : 2,
		    percentageInnerCutout : 0, // This is 0 for Pie charts
		    animationSteps : 100,
		    animationEasing : "easeOutBounce",
		    animateRotate : true,
		    animateScale : false,
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		}
	var optionsDoughnut={
		    segmentShowStroke : true,
		    segmentStrokeColor : "#fff",
		    segmentStrokeWidth : 2,
		    percentageInnerCutout : 50, // This is 0 for Pie charts
		    animationSteps : 100,
		    animationEasing : "easeOutBounce",
		    animateRotate : true,
		    animateScale : false,
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		}
	function setUserHeaderNumber(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata);
	        	var chartdata= new Array();
	        	var result=0;
	        	for (var i=0;i<labels.length;i++){
	        		result = result + jdata[labels[i]];
	        		//chartdata[i]={
	                  //      value: jdata[labels[i]],
	                        //color:"rgba("+colores[i]+",0.5)",
	                        //highlight: "rgba("+colores[i]+",0.75)",
	                    //    label: labels[i]
	                 //}
		        	if (!result || result < 0){
		        		result=0;
		        	}
	        		
	        		document.getElementById(kpi.htmlid).innerHTML=result;
	        	}
	        	console.log(chartdata);
	        	//createChart(kpi.id, "Pie", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	function setUserHeaderNumberArray(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata);
	        	var chartdata= new Array();
	        	var result=0;
	        	for (var i=0;i<labels.length;i++){
	        		if (labels[i] == "datasets" ){
	          			var arryobj=jdata[labels[i]];
	        			for (var j=0;j<arryobj.length;j++){
	        				var obj = arryobj[j];
	        				var values = obj.values;
	        				for (var k=0;k<values.length;k++){
	        					result = result + values[k];
	        				}
	        			}
	        			
	        			
	        		}
	        		else if (labels[i] == "IdeaPublished"){
	        			result = result + jdata[labels[i]];
	        		}
	        		//result = result + jdata[labels[i]];
	        		//chartdata[i]={
	                  //      value: jdata[labels[i]],
	                        //color:"rgba("+colores[i]+",0.5)",
	                        //highlight: "rgba("+colores[i]+",0.75)",
	                    //    label: labels[i]
	                 //}
	        		

	        	}
	        	if (!result || result < 0){
	        		result=0;
	        	}
	        	
        		document.getElementById(kpi.htmlid).innerHTML=result;
	        	
	        	console.log(chartdata);
	        	//createChart(kpi.id, "Pie", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	function createChart(id, type, text, data, legend, color){
		if (currentCharts.indexOf(id)<0){
			currentCharts.push(id);
		}
		if (legend==null){
			legend='';
		}
		
		console.log(data);
		if (data==null){
			data=fakedata
		}
		var contenedor=document.getElementById("contenedorCharts");
		if (document.getElementById("cont"+id)==null){
			contenedor.insertAdjacentHTML('beforeend',"<div class='div33 chartContainer'><div class='chartCabecera' style='background-color:"+color+"'><div class='iconCerrar' onclick='removeKpi(this, \""+id+"\")'></div>"+text+"</div>"+legend+"<div id='cont"+id+"' class='chartBody'></div></div>");
		}
		var width=document.getElementById("cont"+id).offsetWidth-20;
		
		if (type=='Bar'||type==null){
			document.getElementById("cont"+id).innerHTML="<canvas id='cv"+id+"' width='"+width+"' height='200' ></canvas>";
			if (data.labels.length!=0){
				var ctx = document.getElementById('cv'+id).getContext("2d");
				new Chart(ctx).Bar(data, optionsBar);
			}else{
				document.getElementById('cont'+id).innerHTML="No data available for this chart.";
			}
		}
		if (type=='Line'){
			document.getElementById("cont"+id).innerHTML="<canvas id='cv"+id+"' width='"+width+"' height='200' ></canvas>";
			if (data.labels.length!=0){
				var ctx = document.getElementById('cv'+id).getContext("2d");
				new Chart(ctx).Line(data, optionsLine);
			}else{
				document.getElementById('cont'+id).innerHTML="No data available for this chart.";
			}
		}
		if (type=='Radar'){
			document.getElementById("cont"+id).innerHTML=faketable+"<canvas id='cv"+id+"' width='"+(width/2)+"' height='200' ></canvas>";
			var ctx = document.getElementById('cv'+id).getContext("2d");
			new Chart(ctx).Radar(fakedataline, optionsRadar);
		}
		if (type=='Pie'){
			document.getElementById("cont"+id).innerHTML=createDataTable(data)+"<canvas id='cv"+id+"' width='"+(width/2)+"' height='200' ></canvas>";
			if (data.length!=0){
				var ctx = document.getElementById('cv'+id).getContext("2d");
				new Chart(ctx).Pie(data, optionsPie);
			}else{
				document.getElementById('cont'+id).innerHTML="No data available for this chart.";
			}
		}
		if (type=='Doughnut'){
			document.getElementById("cont"+id).innerHTML=faketable+"<canvas id='cv"+id+"' width='"+(width/2)+"' height='200' ></canvas>";
			var ctx = document.getElementById('cv'+id).getContext("2d");
			new Chart(ctx).Doughnut(fakedatapie, optionsDoughnut);
		}
		
	}
	
	function removeKpi(obj, kpi){
		obj.parentElement.parentElement.remove();
		currentCharts.splice(currentCharts.indexOf(kpi), 1);
	}
	
	function setHeaderNumber(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	var result=jdata[pilot[kpi.pilottype]];
	        	if (!result || result < 0){
	        		result=0;
	        	}
	        	document.getElementById(kpi.htmlid).innerHTML=result;
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	function setHeaderNumberTotalDATASET(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	    		if (currentCharts.indexOf(kpi.id)<0){
	    			currentCharts.push(kpi.id);
	    		}
	        	
	        	var labels=Object.keys(jdata)
	        	var data=getObjValues(jdata, labels);
	        	if (labels.length!=0){
			        for (var i =0; i<labels.length;i++){
			        	if (labels[i]=="Dataset"){
			        		if (data[i] < 0){
			        			document.getElementById(kpi.htmlid).innerHTML="0";
			        		}
			        		else{
			        			document.getElementById(kpi.htmlid).innerHTML=data[i];
			        		}
			        	}
		        	}
		        }else{
		        	document.getElementById(kpi.htmlid).innerHTML="0";
				}

	        	
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al setHeaderNumberTotalDATASET!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}		
	function setHeaderNumberTotal(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	var labels=Object.keys(jdata);
	        	var result=0;
	        	for (var i=0; i<labels.length;i++){
	        		result+=jdata[labels[i]];
	        	}
	        	if (!result || result < 0){
	        		result=0;
	        	}	        	
	        	document.getElementById(kpi.htmlid).innerHTML=result;
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	
	function create1DBarChart(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata);
	        	var data=getObjValues(jdata, labels);
	        	var rcolor=kpi.color.substring(1, 3);
	        	var gcolor=kpi.color.substring(3, 5);
	        	var bcolor=kpi.color.substring(5, 7);
	        	
	        	rcolor= parseInt(rcolor,16);
	        	gcolor= parseInt(gcolor,16);
	        	bcolor= parseInt(bcolor,16);

	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: [
	        		        {
	        		            label: "Test",
	        		            fillColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.5)",
	        		            strokeColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.8)",
	        		            highlightFill: "rgba("+rcolor+","+gcolor+","+bcolor+",0.75)",
	        		            highlightStroke: "rgba("+rcolor+","+gcolor+","+bcolor+",1)",
	        		            data: data
	        		        }
	        		    ]
	        		};       
	        	createChart(kpi.id, "Bar", msgLabels[kpi.id], chartdata,"", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	
	
	function create1DLineChartMonth(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var dates=jdata.date[Object.keys(jdata.date)[0]];
	        	var labels=[];
	        	var data=[];
	        	if (dates!=null){
		        	for (var i =0; i<dates.length;i++){
		        		labels[i]=Object.keys(dates[i])[0];
		        		data[i]=dates[i][Object.keys(dates[i])[0]];
		        	}
	        	}
	        	var rcolor=kpi.color.substring(1, 3);
	        	var gcolor=kpi.color.substring(3, 5);
	        	var bcolor=kpi.color.substring(5, 7);
	        	
	        	rcolor= parseInt(rcolor,16);
	        	gcolor= parseInt(gcolor,16);
	        	bcolor= parseInt(bcolor,16);

	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: [
	        		        {
	        		            label: "Test",
	        		            fillColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.5)",
	        		            strokeColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.8)",
	        		            highlightFill: "rgba("+rcolor+","+gcolor+","+bcolor+",0.75)",
	        		            highlightStroke: "rgba("+rcolor+","+gcolor+","+bcolor+",1)",
	        		            data: data
	        		        }
	        		    ]
	        		};       
	        	createChart(kpi.id, "Line", msgLabels[kpi.id], chartdata,"",  kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
		
	function create1DBarChartTop(kpi, num){
	    $.ajax({
	        url: kpi.url,
	        url: urlDate(kpi.url),
	        dataType: 'json',
	        success: function (jdata) {
	        	if (num==null){
	        		num=5;
	        	}
	        	jdata=topList(jdata,num);
//	        	console.log(jdata);
	        	var labels=Object.keys(jdata)
	        	var data=getObjValues(jdata, labels);
	        	var rcolor=kpi.color.substring(1, 3);
	        	var gcolor=kpi.color.substring(3, 5);
	        	var bcolor=kpi.color.substring(5, 7);
	        	
	        	rcolor= parseInt(rcolor,16);
	        	gcolor= parseInt(gcolor,16);
	        	bcolor= parseInt(bcolor,16);

	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: [
	        		        {
	        		            label: "Test",
	        		            fillColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.5)",
	        		            strokeColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.8)",
	        		            highlightFill: "rgba("+rcolor+","+gcolor+","+bcolor+",0.75)",
	        		            highlightStroke: "rgba("+rcolor+","+gcolor+","+bcolor+",1)",
	        		            data: data
	        		        }
	        		    ]
	        		};         
	        	createChart(kpi.id, "Bar", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	function create2DBarChartOld(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata)
	        	var datasets=[];
        		var datasetobj=jdata[labels[0]];
        		var dlabels=Object.keys(datasetobj);
	        	var legend='';
	        	var legendWidth=(100/dlabels.length-2)+'%';
	        	for (var i =0; i<dlabels.length;i++){
	        		var data=[];
	        		for(var j =0; j<labels.length;j++){
	        			data[j]=jdata[labels[j]][dlabels[i]];
	        		}
	        		datasets[i]={
	    		            label: dlabels[i],
	    		            fillColor: "rgba("+colores[i]+",0.5)",
	    		            strokeColor: "rgba("+colores[i]+",0.8)",
	    		            highlightFill: "rgba("+colores[i]+",0.75)",
	    		            highlightStroke: "rgba("+colores[i]+",1)",
	    		            data: data
	    		        }
	        		legend+='<div class="legendField" style="background-color:rgb('+colores[i]+'); width:'+legendWidth+'">'+datasets[i].label+'</div>';
	        	}
	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: datasets
	        		};       
	        	createChart(kpi.id, "Bar", msgLabels[kpi.id], chartdata, legend, kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	
	function create2DBarChartOldPercentage(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata)
	        	var datasets=[];
        		var datasetobj=jdata[labels[0]];
        		var dlabels=Object.keys(datasetobj);
	        	var legend='';
	        	var legendWidth=(100/dlabels.length-2)+'%';
        		var totales=[];
	        	for (var i =0; i<dlabels.length;i++){
	        		var data=[];
	        		for(var j =0; j<labels.length;j++){
	        			data[j]=jdata[labels[j]][dlabels[i]];
	        			if (!totales[j]){
	        				totales[j]=0;
	        			}
	        			totales[j]+=jdata[labels[j]][dlabels[i]];
	        		}
	        		datasets[i]={
	    		            label: dlabels[i]+" %",
	    		            fillColor: "rgba("+colores[i]+",0.5)",
	    		            strokeColor: "rgba("+colores[i]+",0.8)",
	    		            highlightFill: "rgba("+colores[i]+",0.75)",
	    		            highlightStroke: "rgba("+colores[i]+",1)",
	    		            data: data
	    		        }
	        		legend+='<div class="legendField" style="background-color:rgb('+colores[i]+'); width:'+legendWidth+'">'+datasets[i].label+'</div>';
	        	}
	        	for (var i =0; i<datasets.length;i++){
	        		for(var j =0; j<datasets[i].data.length;j++){
	        			datasets[i].data[j]=(datasets[i].data[j]/totales[j]*100).toFixed(2);
	        		}
	        		
	        	}
	        	
	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: datasets
	        		};       
	        	createChart(kpi.id, "Bar", msgLabels[kpi.id], chartdata, legend, kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	
	function setHeaderNumberDATASET(kpi){
		$.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var result = 0;   
	        	for (var i =0; i<jdata.datasets.length;i++){
	        		
	        		if (jdata.datasets[i].label == "Dataset"){
	        			var values = jdata.datasets[i].values;
	        			console.log(values);
	        			for (var v=0;v<values.length;v++){
	    	        		
	    		            result =result+ values[v];
	        			}
	    		   
	        		}
	        	}
	        	document.getElementById(kpi.htmlid).innerHTML=result;

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create2DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });			
	}
	function create2DBarChart(kpi){
		$.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=jdata.labels
	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: []
	        		};   
	        	var legend='';
	        	var legendWidth=(100/jdata.datasets.length-2)+'%';
	        	for (var i =0; i<jdata.datasets.length;i++){
	        		chartdata.datasets[i]={
	    		            label: jdata.datasets[i].label,
	    		            fillColor: "rgba("+colores[i]+",0.5)",
	    		            strokeColor: "rgba("+colores[i]+",0.8)",
	    		            highlightFill: "rgba("+colores[i]+",0.75)",
	    		            highlightStroke: "rgba("+colores[i]+",1)",
	    		            data: jdata.datasets[i].values
	    		        }
	        		legend+='<div class="legendField" style="background-color:rgb('+colores[i]+'); width:'+legendWidth+'">'+jdata.datasets[i].label+'</div>';
	        	}
	        	console.log(legend);
	        	console.log(chartdata);
	        	createChart(kpi.id, "Bar", msgLabels[kpi.id], chartdata, legend, kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create2DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	
	function createAVGBarChart(kpi){
		$.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=jdata.labels
	        	var data = getAVGData(jdata.datasets, jdata.labels.length)
	        	var rcolor=kpi.color.substring(1, 3);
	        	var gcolor=kpi.color.substring(3, 5);
	        	var bcolor=kpi.color.substring(5, 7);
	        	
	        	rcolor= parseInt(rcolor,16);
	        	gcolor= parseInt(gcolor,16);
	        	bcolor= parseInt(bcolor,16);

	        	var chartdata = {
	        		    labels: labels,
	        		    datasets: [
	        		        {
	        		            label: "Test",
	        		            fillColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.5)",
	        		            strokeColor: "rgba("+rcolor+","+gcolor+","+bcolor+",0.8)",
	        		            highlightFill: "rgba("+rcolor+","+gcolor+","+bcolor+",0.75)",
	        		            highlightStroke: "rgba("+rcolor+","+gcolor+","+bcolor+",1)",
	        		            data: data
	        		        }
	        		    ]
	        		};         
	        	createChart(kpi.id, "Bar", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create2DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	
	function getAVGData(obj, length){
		var sum=[];
		var total=[];
		for (var i=0; i<length;i++){//inicializar
			sum[i]=0;
			total[i]=0;
		}
		for (var i=0; i<obj.length;i++){//rellenar
			for (var j=0; j<obj[i].values.length;j++){
				sum[j]+=parseInt(obj[i].label)*obj[i].values[j];
				total[j]+=obj[i].values[j];
			}
		}
		var retorno=[];
		for (var i=0; i<length;i++){//inicializar
			retorno[i]=sum[i]/total[i];
		}
		return retorno;
	}
	
	function createPieChart(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
//	        	console.log(jdata);
	        	var labels=Object.keys(jdata);
	        	var chartdata= new Array();
	        	for (var i=0;i<labels.length;i++){
	        		chartdata[i]={
	                        value: jdata[labels[i]],
	                        color:"rgba("+colores[i]+",0.5)",
	                        highlight: "rgba("+colores[i]+",0.75)",
	                        label: labels[i]
	                    }
	        	}
	        	console.log(chartdata);
	        	createChart(kpi.id, "Pie", msgLabels[kpi.id], chartdata, "", kpi.color);

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al create1DBarChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}
	
	function createDataTable(data){
		var table='<div class="tablaDatos">';    	
		for (var i=0;i<data.length;i++){
			table+=''+
			'	<div class="lineaDato">'+
			'		<div class="valorDato">'+data[i].value+'</div>'+
			'		<div class="tituloDato">'+data[i].label+'</div>'+
			'	</div>';
    		
    	}
		table+='</div>';
		return table;
	}		
	
	function createNumberChart(kpi){
	    $.ajax({
	        url: urlDate(kpi.url),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	    		if (currentCharts.indexOf(kpi.id)<0){
	    			currentCharts.push(kpi.id);
	    		}
	        	
	        	var labels=Object.keys(jdata)
	        	var data=getObjValues(jdata, labels);
	    		var obj='';
	        	var contenedor=document.getElementById("contenedorCharts");
	    		if (document.getElementById("cont"+kpi.id)==null){
	    			obj='<div id="cont'+kpi.id+'" class="div33 chartContainer"></div>';
	    			contenedor.insertAdjacentHTML('beforeend',obj);
	    		}
	    		contenedor=document.getElementById("cont"+kpi.id);
	    		contenedor.innerHTML='';	        	
	        	
	        	obj=''+
				'<div class="chartCabecera"><div class="iconCerrar" onclick="removeKpi(this, \''+kpi.id+'\')"></div>'+msgLabels[kpi.id]+'</div>'+
				'	<div class="chartBody">';
		        if (labels.length!=0){
			        for (var i =0; i<labels.length;i++){
		        		obj+=''+
						'<div style="float:left;width:'+(100/labels.length)+'%">'+
						'<div class="contenedorValue">'+
						'	<div class="cabeceraValue">'+labels[i]+'</div>'+
						'	<div class="valorValue">'+data[i]+'</div>'+
						'	<div class="unidadesValue">NUMBER</div>'+
						'</div>'+
						'</div>';
		        	}
		        }else{
		        	obj+="No data available for this chart.";
				}

	        	obj+=''+
				'		</div>'+
				'	</div>';
	        	
	        	contenedor.insertAdjacentHTML('beforeend',obj);
	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al test2DBar!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	
	function createObjetiveChart(kpi){
		if(kpi.custom==undefined){
			kpi.custom="";
		}
	    $.ajax({
	        url: urlDate(kpi.url+"?type="+kpi.retype+"&app="+kpi.app+"&custom="+kpi.custom),
	        type: 'GET',
	        dataType: 'json',
	        success: function (jdata) {
	        	console.log(jdata);
	        	var labels=Object.keys(jdata)
	        	var data=getObjValues(jdata, labels);
	        	drawObjetiveChart(kpi.id, jdata[kpi.app], kpi.objetive, msgLabels[kpi.id], kpi.retype)

	      },
	      error: function (XMLHttpRequest, textStatus, errorThrown) {
	          console.log('Error al createObjetiveChart!!');
	          console.log(JSON.stringify(XMLHttpRequest));
	          console.log(JSON.stringify(textStatus));
	          console.log(JSON.stringify(errorThrown));
	        }
	    });	
	}	
	
	function drawObjetiveChart(id, number, objetive, title, type){
		if (currentCharts.indexOf(id)<0){
			currentCharts.push(id);
		}
		
		if (number==undefined){
			number=0;
		}
		var obj='';
    	var contenedor=document.getElementById("contenedorCharts");
		if (document.getElementById("cont"+id)==null){
			obj='<div id="cont'+id+'" class="div33 chartContainer"></div>';
			contenedor.insertAdjacentHTML('beforeend',obj);
		}
		contenedor=document.getElementById("cont"+id);
		contenedor.innerHTML='';

		obj=''+
		'<div class="chartCabecera"><div class="iconCerrar"  onclick="removeKpi(this, \''+id+'\');"></div>'+title+'</div>'+
		'	<div class="chartBody">';
    	if (number<objetive){
    		var height=number/objetive*100;
    		var bottom=height;
    		if (height>80){
    			bottom=80;
    		}
    	obj+=''+
        	'<div class="topTextObjetive">Objetive: '+objetive+'</div>'+
        	'<div class="topObjetive">'+
        	'	<div style="bottom:'+bottom+'%" class="lowTextObjetive">'+type+': '+number+'</div>'+
        	'	<div style="height:'+height+'%" class="lowObjetive"></div>'+
        	'</div>';
    	}else{
    		var height=objetive/number*100;
    		var bottom=height;
    		if (height>80){
    			bottom=80;
    		}
        	obj+=''+
        	'<div class="topTextObjetive">'+type+': '+number+'</div>'+
        	'<div class="topObjetive">'+
        	'	<div style="bottom:'+bottom+'%" class="lowTextObjetive">Objetive: '+objetive+'</div>'+
        	'	<div style="height:'+height+'%" class="lowObjetive"></div>'+
        	'</div>';	        		
    	}
    	
    	obj+=''+
		'		</div>'+
		'	</div>';
    	contenedor.insertAdjacentHTML('beforeend',obj);
	}	
	
function testConnection(){
	var url='https://dev.welive.eu/dev/api/ads/cdv/stakeholders';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (jdata) {
        	console.log(jdata);
        	var labels=Object.keys(jdata)
        	var data=getObjValues(jdata, labels);
        	var chartdata = {
        		    labels: labels,
        		    datasets: [
        		        {
        		            label: "Test",
        		            fillColor: "rgba(151,187,205,0.5)",
        		            strokeColor: "rgba(151,187,205,0.8)",
        		            highlightFill: "rgba(151,187,205,0.75)",
        		            highlightStroke: "rgba(151,187,205,1)",
        		            data: data
        		        }
        		    ]
        		};       
        	createChart("test", "Bar", "Registered Users", chartdata, "", kpi.color);

      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log('Error al testConnection!!');
          console.log(JSON.stringify(XMLHttpRequest));
          console.log(JSON.stringify(textStatus));
          console.log(JSON.stringify(errorThrown));
        }
    });	
}

function test1D(){
	var url='http://localhost:8080/welive/api/ads/oia/ideainimplementation/count';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (jdata) {
        	console.log(jdata);
        	var labels=Object.keys(jdata)
        	var data=getObjValues(jdata, labels);
        	var chartdata = {
        		    labels: labels,
        		    datasets: [
        		        {
        		            label: "Test",
        		            fillColor: "rgba(151,187,205,0.5)",
        		            strokeColor: "rgba(151,187,205,0.8)",
        		            highlightFill: "rgba(151,187,205,0.75)",
        		            highlightStroke: "rgba(151,187,205,1)",
        		            data: data
        		        }
        		    ]
        		};       
        	createChart("test1D", "Bar", "Registered Users", chartdata, "", kpi.color);

      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log('Error al testConnection!!');
          console.log(JSON.stringify(XMLHttpRequest));
          console.log(JSON.stringify(textStatus));
          console.log(JSON.stringify(errorThrown));
        }
    });	
}


function test2DBar(){
	var url='http://localhost:8080/welive/api/ads/oia/idea/search/all/materializedcity?withmonths=false';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (jdata) {
        	console.log(jdata);
        	var labels=jdata.labels
        	var chartdata = {
        		    labels: labels,
        		    datasets: []
        		};   
        	for (var i =0; i<jdata.datasets.length;i++){
        		chartdata.datasets[i]={
    		            label: jdata.datasets[i].label,
    		            fillColor: "rgba("+colores[i]+",0.5)",
    		            strokeColor: "rgba("+colores[i]+",0.8)",
    		            highlightFill: "rgba("+colores[i]+",0.75)",
    		            highlightStroke: "rgba("+colores[i]+",1)",
    		            data: jdata.datasets[i].values
    		        }
        	}
        	console.log(chartdata);
        	createChart("test2D", "Bar", "Number of Artefacts", chartdata, "", kpi.color);

      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log('Error al test2DBar!!');
          console.log(JSON.stringify(XMLHttpRequest));
          console.log(JSON.stringify(textStatus));
          console.log(JSON.stringify(errorThrown));
        }
    });	
}

function test1DNumber(title){
	var url='http://localhost:8080/welive/api/ads/oia/idea/search/all/materialized?pilot=Bilbao';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (jdata) {
        	var labels=Object.keys(jdata)
        	var data=getObjValues(jdata, labels);
        	var obj=''+
			'<div class="div33 chartContainer">'+
			'<div class="chartCabecera"><div class="iconCerrar" onclick="this.parentElement.parentElement.remove();"></div>'+title+'</div>'+
			'	<div id="contkpi0" class="chartBody">';
        	for (var i =0; i<labels.length;i++){
        		obj+=''+
				'<div style="float:left;width:'+(100/labels.length)+'%">'+
				'<div class="contenedorValue">'+
				'	<div class="cabeceraValue">'+labels[i]+'</div>'+
				'	<div class="valorValue">'+data[i]+'</div>'+
				'	<div class="unidadesValue">NUMBER</div>'+
				'</div>'+
				'</div>';
        	}

        	obj+=''+
			'		</div>'+
			'	</div>'+
			'</div>';
        	var contenedor=document.getElementById("contenedorCharts");
        	contenedor.insertAdjacentHTML('beforeend',obj);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log('Error al test2DBar!!');
          console.log(JSON.stringify(XMLHttpRequest));
          console.log(JSON.stringify(textStatus));
          console.log(JSON.stringify(errorThrown));
        }
    });	
}

//https://stackoverflow.com/questions/16086162/handle-file-download-from-ajax-post
function getInAppData(pilot,bearer){
	var url='https://in-app.cloudfoundry.welive.eu/api/questionnaire/get-responses?pilotId='+pilot;
    $.ajax({
        url: url,
        headers: {
            'Authorization':'Bearer '+bearer//4e380c76-a96a-4beb-8fc7-2e7eecfd3a44'
        },        
        type: 'GET',
        //dataType: 'application/octet-stream',
        success: function (response, status, xhr) {
            var filename = "responses.zip";
            var disposition = xhr.getResponseHeader("content-disposition");
            var all = xhr.getAllResponseHeaders();
            
            console.log("all:"+all);

            console.log("filemane:"+filename);
            var type = xhr.getResponseHeader('content-type');
            console.log("type:"+type);
            var blob = new Blob([response], { type: type });

            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                window.navigator.msSaveBlob(blob, filename);
            } else {
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);
                if (filename) {
                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }
                } else {
                    window.location = downloadUrl;
                }

            }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          console.log('Error al getInAppData!!');
          console.log(JSON.stringify(XMLHttpRequest));
          console.log(JSON.stringify(textStatus));
          console.log(JSON.stringify(errorThrown));
        }
    });
}

function getObjValues(jdata, arr){
	var retorno=new Array();
	for (var i=0;i<arr.length;i++){
		retorno.push(jdata[arr[i]]);
	}
	return retorno;
}

function topList(obj, num){
	var retorno={};
	var keysSorted = Object.keys(obj).sort(function(a,b){return obj[b]-obj[a]});
	console.log(keysSorted);
	
	for (var i=0; i<keysSorted.length;i++){
		retorno[keysSorted[i]]=obj[keysSorted[i]];
		if (i>num-2)
			return retorno;
	}
	return retorno;
}
function urlDate(url){
	//yyyy-MM-dd HH:mm:ss.SSS
	var params = "";
	var union="?";
	if (url.indexOf("?")>0){
		union="&";
	}
	
	if (document.getElementById("fromDate").value!=""){
		params+=union+"from="+document.getElementById("fromDate").value+" 0:00:00.000";
		union="&";
	}
	if (document.getElementById("toDate").value!=""){
		params+=union+"to="+document.getElementById("toDate").value+" 23:59:59.000";
		union="&";
	}
	console.log(url + params);
	return url + params;
}

function seleccionarPerfil(perfil){
	console.log ("perfil: "+perfil);
	var objs=null;
	if (perfil==0){
		objs=document.getElementsByClassName("userciudad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("usersuper");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("userciudadano");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
	}	
	if (perfil==1){
		objs=document.getElementsByClassName("userciudadano");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("userciudad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usersuper");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
	}
	if (perfil==2){
		objs=document.getElementsByClassName("userciudadano");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usersuper");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("userciudad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
	}
	if (perfil==3){
		objs=document.getElementsByClassName("userciudad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usersuper");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("userciudadano");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
	}
	
}
function seleccionarCuidad(ciudad){
	console.log ("ciudad: "+ciudad);
	var objs=null;
	if (ciudad=="All"){
		objs=document.getElementsByClassName("userbilbao");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("usertrento");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("useruusima");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("usernovisad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
	}	
	if (ciudad=="Bilbao"){
		objs=document.getElementsByClassName("userbilbao");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("usertrento");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("useruusima");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usernovisad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
	}
	if (ciudad=="Trento"){
		objs=document.getElementsByClassName("userbilbao");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usertrento");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("useruusima");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usernovisad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
	}
	if (ciudad=="Uusimaa"){
		objs=document.getElementsByClassName("userbilbao");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usertrento");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("useruusima");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
		objs=document.getElementsByClassName("usernovisad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
	}
	if (ciudad=="Novisad"){
		objs=document.getElementsByClassName("userbilbao");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usertrento");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("useruusima");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="none";
		}
		objs=document.getElementsByClassName("usernovisad");
		for (var i=0; i<objs.length;i++){
			objs[i].style.display="block";
		}
	}
	
}
function changeFatherOverflow(obj){
	if(obj.parentElement.style.height=="initial"){
		obj.parentElement.style.height="35px";
	}else{
		obj.parentElement.style.height="initial";
	}
	
}

function redrawCharts(){
	for (var i=0;i<currentCharts.length;i++){
		kpi=kpidata[currentCharts[i]];
		if (kpi.type=='Bar'){
			create1DBarChart(kpi);
		}
		if (kpi.type=='BarOld'){
			create2DBarChartOld(kpi);
		}
		if (kpi.type=='BarOldPerc'){
			create2DBarChartOldPercentage(kpi);
		}
		if (kpi.type=='BarTop'){
			create1DBarChartTop(kpi);
		}
		if (kpi.type=='Bar2D'){
			create2DBarChart(kpi);
		}
		if (kpi.type=='Pie'){
			createPieChart(kpi);
		}
		if (kpi.type=='Line'){
			create1DLineChartMonth(kpi);
		}
		if (kpi.type=='Number'){
			createNumberChart(kpi);
		}
		if (kpi.type=='Objetive'){
			createObjetiveChart(kpi);
		}
		if (kpi.type=='Avg'){
			createAVGBarChart(kpi);
		}
	}
	if (role.indexOf("OnmiAdmin")>-1){
		setHeaderNumberTotal(kpidata.kpi1header);
		setHeaderNumberTotal(kpidata.kpi7header);
		setHeaderNumberTotal(kpidata.kpi9header);
		setHeaderNumberTotal(kpidata.kpi11header);
		setHeaderNumberDATASET(kpidata.kpi14headerDATASETALL);		

	}
	else if (role.indexOf("Authority")>-1){
		setHeaderNumber(kpidata.kpi1header);
		setHeaderNumber(kpidata.kpi7header);
		setHeaderNumber(kpidata.kpi9header);
		setHeaderNumber(kpidata.kpi11header);
		setHeaderNumberTotalDATASET(kpidata.kpi14headerDATASET);				

	}
	else if (role.indexOf("Authority")==-1 && role.indexOf("Authority") ==-1){
		setUserHeaderNumber(kpidata.kpi3);
		setUserHeaderNumberArray(kpidata.kpi6);
		setUserHeaderNumber(kpidata.kpi8);
		setUserHeaderNumberArray(kpidata.kpi10);
		setUserHeaderNumber(kpidata.kpi12);
		setUserHeaderNumber(kpidata.kpi18);
	}
	
}

function showMenu(){
	$('.menulateral').css({'left':  -300});
	document.getElementById("menulateral").style.display='block';
	$('.menulateral').animate({'left':  '0'}, function(){
		$('.cerrarMenu').css({'display':  'block'});
    });	
}

function closeMenu(){
	$('.cerrarMenu').css({'display':  'none'});
	$('.menulateral').animate({'left':  '-300'}, function(){
		document.getElementById("menulateral").style.display='none';
    });
}
