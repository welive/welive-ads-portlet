//var baseurl="http://localhost:8080/welive/api"
var baseurl="https://dev.welive.eu/dev/api"
//var baseurl="https://test.welive.eu/dev/api"

	$( document ).ready(function() {
		try{
		
		if (role.indexOf("User")>-1){
			seleccionarPerfil(3);
		}
		if (role.indexOf("OnmiAdmin")>-1){
			document.getElementById("titGeneral").innerHTML=msgLabels["lbl1"];
			setHeaderNumberTotal(kpidata.kpi1header);
			setHeaderNumberTotal(kpidata.kpi7header);
			setHeaderNumberTotal(kpidata.kpi9header);
			setHeaderNumberTotal(kpidata.kpi11header);
			setHeaderNumberDATASET(kpidata.kpi14headerDATASETALL);		
			seleccionarPerfil(1);
			seleccionarCuidad("All");
		}
		else if (role.indexOf("Authority")>-1){
			document.getElementById("titGeneral").innerHTML==msgLabels["lbl2"]+" "+pilot[0]+" "+msgLabels["lbl3"];
			setHeaderNumber(kpidata.kpi1header);
			setHeaderNumber(kpidata.kpi7header);
			setHeaderNumber(kpidata.kpi9header);
			setHeaderNumber(kpidata.kpi11header);
			setHeaderNumberTotalDATASET(kpidata.kpi14headerDATASET);				
			seleccionarPerfil(2);
			seleccionarCuidad(pilot[0]);
		}
		else if (role.indexOf("Authority")==-1 && role.indexOf("Authority") ==-1){
			setUserHeaderNumber(kpidata.kpi3);
			setUserHeaderNumberArray(kpidata.kpi6);
			setUserHeaderNumber(kpidata.kpi8);
			setUserHeaderNumberArray(kpidata.kpi10);
			setUserHeaderNumber(kpidata.kpi12);
			setUserHeaderNumber(kpidata.kpi18);
			createPieChart(kpidata.kpi12);
			create1DLineChartMonth(kpidata.kpi10);
			createPieChart(kpidata.kpi8);
			createPieChart(kpidata.kpi3);
			create2DBarChart(kpidata.kpi6);
			createPieChart(kpidata.kpi18);
			seleccionarPerfil(3);
		}
		}
		catch(e){
			
		}
	});	

var kpidata={
		kpi432:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users.(Target>50)",
			type:"Objetive",
			retype:"newUser", 
			app:"helsinkicitymuseums",
			objetive:50,
			pilottype:0,
			id:"kpi432"
		},
		kpi431:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of queries made by app users. (Target>150)",
			type:"Objetive",
			retype:"UserQuery", 
			app:"helsinkicitymuseums",
			objetive:150,
			pilottype:0,
			id:"kpi431"
		},
		kpi430:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started: > 100 ",
			type:"Objetive",
			retype:"AppStart", 
			app:"helsinkicitymuseums",
			objetive:100,
			pilottype:0,
			id:"kpi430"
		},	
		kpi422:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users.(Target>50)",
			type:"Objetive",
			retype:"newUser", 
			app:"helsinkicitybikes",
			objetive:50,
			pilottype:0,
			id:"kpi422"
		},		
		kpi421:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of queries made by app users. (Target>150)",
			type:"Objetive",
			retype:"UserQuery", 
			app:"helsinkicitybikes",
			objetive:150,
			pilottype:0,
			id:"kpi421"
		},
		kpi420:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started: > 100 ",
			type:"Objetive",
			retype:"AppStart", 
			app:"helsinkicitybikes",
			objetive:100,
			pilottype:0,
			id:"kpi420"
		},	
		kpi413:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users.(Target>50)",
			type:"Objetive",
			retype:"newUser", 
			app:"sportit",
			objetive:50,
			pilottype:0,
			id:"kpi413"
		},		
		kpi412:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of comments made by app users. > 10",
			type:"Objetive",
			retype:"UserComment", 
			app:"sportit",
			objetive:10,
			pilottype:0,
			id:"kpi412"
		},
		kpi411:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of queries made by app users. (Target>150)",
			type:"Objetive",
			retype:"UserQuery", 
			app:"sportit",
			objetive:150,
			pilottype:0,
			id:"kpi411"
		},
		kpi410:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started: > 100 ",
			type:"Objetive",
			retype:"AppStart", 
			app:"sportit",
			objetive:100,
			pilottype:0,
			id:"kpi410"
		},			
		kpi401:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of queries made by app users: > 150",
			type:"Objetive",
			retype:"AppPersonalized", 
			app:"culturekey",
			objetive:150,
			pilottype:0,
			id:"kpi401"
		},
		kpi400:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started: > 100 ",
			type:"Objetive",
			retype:"AppStarted", 
			app:"culturekey",
			objetive:100,
			pilottype:0,
			id:"kpi400"
		},			
		kpi391:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of queries made by app users: > 150 ",
			type:"Objetive",
			retype:"AppPersonalized", 
			app:"localcommunity",
			objetive:150,
			pilottype:0,
			id:"kpi391"
		},
		kpi390:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started: > 100 ",
			type:"Objetive",
			retype:"AppStarted", 
			app:"localcommunity",
			objetive:100,
			pilottype:0,
			id:"kpi390"
		},				
		kpi383:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Percentage of users personalizing the app saving a favourite office or procedure: > 20% ",
			type:"Objetive",
			retype:"PersonalizationEvent", 
			app:"TrentoPAGuide",
			objetive:20,
			pilottype:0,
			id:"kpi383"
		},						
		kpi382:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of contact (email, phone) established with PA office: > 100 ",
			type:"Objetive",
			retype:"PAContactedEvent", 
			app:"TrentoPAGuide",
			objetive:100,
			pilottype:0,
			id:"kpi382"
		},				
		kpi381:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of “bring me there” requests: > 200   ",
			type:"Objetive",
			retype:"DirectionsRequestedEvent", 
			app:"TrentoPAGuide",
			objetive:200,
			pilottype:0,
			id:"kpi381"
		},
		kpi380:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is used/started: > 400 ",
			type:"Objetive",
			retype:"AppStartedEvent", 
			app:"TrentoPAGuide",
			objetive:400,
			pilottype:0,
			id:"kpi380"
		},		
		
		kpi372:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Percentage of users personalizing the app saving a favourite public space",
			type:"Objetive",
			retype:"PersonalizationEvent", 
			app:"TrentoRoomBooking",
			objetive:20,
			pilottype:0,
			id:"kpi372"
		},				
		kpi371:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Percentage of public space reservation: > 50%  ",
			type:"Objetive",
			retype:"RoomReservationEvent", 
			app:"TrentoRoomBooking",
			objetive:50,
			pilottype:0,
			id:"kpi371"
		},
		kpi370:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the user searches for a room: > 150 ",
			type:"Objetive",
			retype:"RoomSearchEvent", 
			app:"TrentoRoomBooking",
			objetive:150,
			pilottype:0,
			id:"kpi370"
		},		
		kpi361:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of searches for information",
			type:"Objetive",
			retype:"SearchEvent", 
			app:"TrentoInformer",
			objetive:500,
			pilottype:0,
			id:"kpi361"
		},				
		kpi360:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times a notified information is read ",
			type:"Objetive",
			retype:"NotificationReadEvent", 
			app:"TrentoInformer",
			objetive:750,
			pilottype:0,
			id:"kpi360"
		},

		kpi353:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of interactions among attendants to events: 80 ",
			type:"Objetive",
			retype:"AppInterationsEvents", 
			app:"BilbaoEvents",
			objetive:80,
			pilottype:0,
			id:"kpi353"
		},				
		kpi352:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of events proposed by citizens: 10 events ",
			type:"Objetive",
			retype:"AppEventCitizens", 
			app:"BilbaoEvents",
			objetive:10,
			pilottype:0,
			id:"kpi352"
		},
		kpi351:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of registered users. (Target=20)",
			type:"Objetive",
			retype:"AppUserRegistered", 
			app:"BilbaoEvents",
			objetive:50,
			pilottype:0,
			id:"kpi351"
		},		
		kpi350:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"BilbaoEvents",
			objetive:150,
			pilottype:0,
			id:"kpi350"
		},			
		kpi344:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of ratings received by users attending events",
			type:"Objetive",
			retype:"AttendantsEventRated", 
			app:"EntrepreneurshipKlub",
			objetive:20,
			pilottype:0,
			id:"kpi344"
		},	
		kpi343:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of searches for other users available: 200",
			type:"Objetive",
			retype:"ContactsSearched", 
			app:"EntrepreneurshipKlub",
			objetive:200,
			pilottype:0,
			id:"kpi343"
		},				
		kpi342:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of user registrations for events managed by this app  > 100",
			type:"Objetive",
			retype:"UserEventCheckedIn", 
			app:"EntrepreneurshipKlub",
			objetive:100,
			pilottype:0,
			id:"kpi342"
		},
		kpi341:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of registered users. (Target=50)",
			type:"Objetive",
			retype:"AppUserRegistered", 
			app:"EntrepreneurshipKlub",
			objetive:50,
			pilottype:0,
			id:"kpi341"
		},		
		kpi340:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"EntrepreneurshipKlub",
			objetive:150,
			pilottype:0,
			id:"kpi340"
		},		
		kpi331:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personalized info request. (Target>50)",
			type:"Objetive",
			retype:"AppPersonalize", 
			app:"PublicProcurement",
			objetive:50,
			pilottype:0,
			id:"kpi331"
		},	
		kpi330:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"PublicProcurement",
			objetive:150,
			pilottype:0,
			id:"kpi330"
		},
		kpi321:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personalized info request. (Target>50)",
			type:"Objetive",
			retype:"AppPersonalized", 
			app:"RellocationAdvisor",
			objetive:50,
			pilottype:0,
			id:"kpi321"
		},	
		kpi320:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"RellocationAdvisor",
			objetive:150,
			pilottype:0,
			id:"kpi320"
		},
		kpi311:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personalized info request. (Target>50)",
			type:"Objetive",
			retype:"AppPersonalized", 
			app:"SafeCityTrip",
			objetive:50,
			pilottype:0,
			id:"kpi311"
		},	
		kpi310:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"SafeCityTrip",
			objetive:150,
			pilottype:0,
			id:"kpi310"
		},
		kpi301:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personalized info request. (Target>50)",
			type:"Objetive",
			retype:"AppPersonalize", 
			app:"Trento Orari Trasporti",
			objetive:50,
			pilottype:0,
			id:"kpi301"
		},	
		kpi300:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Trento Orari Trasporti",
			objetive:150,
			pilottype:0,
			id:"kpi300"
		},
		kpi291:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personalized info request. (Target>50)",
			type:"Objetive",
			retype:"AppPersonalize", 
			app:"Trento Bike Sharing",
			objetive:50,
			pilottype:0,
			id:"kpi291"
		},	
		kpi290:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Trento Bike Sharing",
			objetive:150,
			pilottype:0,
			id:"kpi290"
		},
		kpi281:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personalized info request. (Target>50)",
			type:"Objetive",
			retype:"AppPersonalize", 
			app:"Trento Street Cleaning",
			objetive:50,
			pilottype:0,
			id:"kpi281"
		},	
		kpi280:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Trento Street Cleaning",
			objetive:150,
			pilottype:0,
			id:"kpi280"
		},
		kpi274:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of top problem queries made by app users. (Target>100)",
			type:"Objetive",
			retype:"StatsView", 
			app:"MyCityMood",
			objetive:100,
			pilottype:0,
			id:"kpi274"
		},	
		kpi273:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of city mood heatmaps added to user dashboard. (Target>100)",
			type:"Objetive",
			retype:"AddItem", 
			app:"MyCityMood",
			objetive:100,
			pilottype:0,
			id:"kpi273"
		},	
		kpi272:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of city mood heatmaps viewed by app users. (Target>200)",
			type:"Objetive",
			retype:"NewQuery", 
			app:"MyCityMood",
			objetive:200,
			pilottype:0,
			id:"kpi272"
		},	
		kpi271:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"MyCityMood",
			objetive:150,
			pilottype:0,
			id:"kpi271"
		},	
		kpi270:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users. (Target>50)",
			type:"Objetive",
			retype:"NewUser", 
			app:"MyCityMood",
			objetive:100,
			pilottype:0,
			id:"kpi270"
		},
		kpi265:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times when match matrix is viewed by app users. (Target>100)",
			type:"Objetive",
			retype:"StatsView", 
			custom: 'AND custom_statstype: "matchMatrix"',
			app:"MyNeighbourhood",
			objetive:100,
			pilottype:0,
			id:"kpi265"
		},		
		kpi264:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of neighbourhood heatmaps viewed by app users. (Target>100)",
			type:"Objetive",
			retype:"StatsView", 
			app:"MyNeighbourhood",
			custom: 'AND custom_statstype: "scoreHeatmap"',
			objetive:100,
			pilottype:0,
			id:"kpi264"
		},	
		kpi263:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of interesting addresses added by app users. (Target>100)",
			type:"Objetive",
			retype:"AddItem", 
			app:"MyNeighbourhood",
			custom: 'AND custom_itemtype: "address"',
			objetive:100,
			pilottype:0,
			id:"kpi263"
		},	
		kpi262:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of personal preferences defined by app users. (Target>100)",
			type:"Objetive",
			retype:"AddItem", 
			custom: 'AND custom_itemtype: "preference"',
			app:"MyNeighbourhood",
			objetive:100,
			pilottype:0,
			id:"kpi262"
		},	
		kpi261:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"MyNeighbourhood",
			objetive:150,
			pilottype:0,
			id:"kpi261"
		},	
		kpi260:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users. (Target>100)",
			type:"Objetive",
			retype:"NewUser", 
			app:"MyNeighbourhood",
			objetive:100,
			pilottype:0,
			id:"kpi260"
		},
		kpi255:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of poll locations added by app users. (Target>100)",
			type:"Objetive",
			retype:"AddItem", 
			app:"MyOpinion",
			custom: 'AND custom_itemtype: "pollLocation"',
			objetive:100,
			pilottype:0,
			id:"kpi255"
		},	
		kpi254:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times when user viewed poll location statistics. (Target>100)",
			type:"Objetive",
			retype:"StatsView", 
			app:"MyOpinion",
			custom: 'AND custom_statstype: "pollStats"',
			objetive:100,
			pilottype:0,
			id:"kpi254"
		},	
		kpi253:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of user opinions given with this app. (Target>200)",
			type:"Objetive",
			retype:"AddItem", 
			app:"MyOpinion",
			custom: 'AND custom_itemtype: "pollAnswer"',
			objetive:200,
			pilottype:0,
			id:"kpi253"
		},	
		kpi252:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times when a user searches for open polls. (Target>200)",
			type:"Objetive",
			retype:"NewQuery", 
			app:"MyOpinion",
			objetive:200,
			pilottype:0,
			id:"kpi252"
		},	
		kpi251:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"MyOpinion",
			objetive:150,
			pilottype:0,
			id:"kpi251"
		},	
		kpi250:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users. (Target>100)",
			type:"Objetive",
			retype:"NewUser", 
			app:"MyOpinion",
			objetive:100,
			pilottype:0,
			id:"kpi250"
		},
		kpi246:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of poll locations added by app users. (Target>100)",
			type:"Objetive",
			retype:"AddItem", 
			app:'MyPolls',
			custom: 'AND custom_itemtype: "pollLocation"',
			objetive:100,
			pilottype:0,
			id:"kpi246"
		},			
		kpi245:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times when app user views poll statistics. (Target>200)",
			type:"Objetive",
			retype:"StatsView", 
			app:"MyPolls",
			objetive:200,
			pilottype:0,
			id:"kpi245"
		},			
		kpi244:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of new polls created with this app. (Target>20)",
			type:"Objetive",
			retype:"AddItem", 
			app:'MyPolls',
			custom: 'AND custom_itemtype: "poll"',
			objetive:20,
			pilottype:0,
			id:"kpi244"
		},			
		kpi243:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of polls added to favorites list. (Target>50)",
			type:"Objetive",
			retype:"AddItem", 
			custom: 'AND custom_itemtype: "favoritePoll"',
			app:"MyPolls",
			objetive:50,
			pilottype:0,
			id:"kpi243"
		},			
		kpi242:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of poll queries made by app users. (Target>50)",
			type:"Objetive",
			retype:"NewQuery", 
			app:"MyPolls",
			objetive:50,
			pilottype:0,
			id:"kpi242"
		},			
		kpi241:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of times the app is started. (Target>150)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"MyPolls",
			objetive:150,
			pilottype:0,
			id:"kpi241"
		},	
		kpi240:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of app users. (Target>100)",
			type:"Objetive",
			retype:"NewUser", 
			app:"MyPolls",
			objetive:100,
			pilottype:0,
			id:"kpi240"
		},	
		kpi234:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number actually executed community works for others. (Target>50)",
			type:"Objetive",
			retype:"CapabilityRequested", 
			app:"Faborenet",
			objetive:50,
			pilottype:0,
			id:"kpi234"
		},		
		kpi233:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of searches for task execution by others of interest. (Target>900)",
			type:"Objetive",
			retype:"CapabilitySearched", 
			app:"Faborenet",
			objetive:900,
			pilottype:0,
			id:"kpi233"
		},			
		kpi232:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of user profile skills and service offers registered. (Target=300)",
			type:"Objetive",
			retype:"CapabilityAdded", 
			app:"Faborenet",
			objetive:300,
			pilottype:0,
			id:"kpi232"
		},			
		kpi231:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of registered users(Target=50)",
			type:"Objetive",
			retype:"AppUserRegistered", 
			app:"Faborenet",
			objetive:50,
			pilottype:0,
			id:"kpi231"
		},	
		kpi230:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>600)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Faborenet",
			objetive:600,
			pilottype:0,
			id:"kpi230"
		},	
		kpi224:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of selected POIs to view details. (Target=600)",
			type:"Objetive",
			retype:"POIsSelected", 
			app:"Bilbon",
			objetive:600,
			pilottype:0,
			id:"kpi224"
		},			
		kpi223:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of searched POIs. (Target=1200)",
			type:"Objetive",
			retype:"POIsSearched", 
			app:"Bilbon",
			objetive:1200,
			pilottype:0,
			id:"kpi223"
		},			
		kpi222:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of new POIs added. (Target=60)",
			type:"Objetive",
			retype:"POIAdded", 
			app:"Bilbon",
			objetive:60,
			pilottype:0,
			id:"kpi222"
		},	
		kpi221:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of registered users. (Target=60)",
			type:"Objetive",
			retype:"AppUserRegistered", 
			app:"Bilbon",
			objetive:60,
			pilottype:0,
			id:"kpi221"
		},	
		kpi220:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>600)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Bilbon",
			objetive:600,
			pilottype:0,
			id:"kpi220"
		},		
		kpi213:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of votes per campaign received. (Target=100)",
			type:"Objetive",
			retype:"Vote4VotingCampaign", 
			app:"Bilbozkatu",
			objetive:100,
			pilottype:0,
			id:"kpi213"
		},		
		kpi212:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of voting campaigns organized. (Target=4)",
			type:"Objetive",
			retype:"VotingCampaignOrganized", 
			app:"Bilbozkatu",
			objetive:4,
			pilottype:0,
			id:"kpi212"
		},			
		kpi211:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of registered users. (Target=50)",
			type:"Objetive",
			retype:"AppUserRegistered", 
			app:"Bilbozkatu",
			objetive:60,
			pilottype:0,
			id:"kpi211"
		},	
		kpi210:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>600)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Bilbozkatu",
			objetive:600,
			pilottype:0,
			id:"kpi210"
		},
		kpi205:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of items requested on loan, sell or donation. (Target=300)",
			type:"Objetive",
			retype:"ItemRequested", 
			app:"Auzonet",
			objetive:300,
			pilottype:0,
			id:"kpi205"
		},			
		kpi204:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of borrowed/bought products. (Target=50)",
			type:"Objetive",
			retype:"ItemBorrowed", 
			app:"Auzonet",
			objetive:50,
			pilottype:0,
			id:"kpi204"
		},			
		kpi203:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of searches for items of interest. (Target=900)",
			type:"Objetive",
			retype:"ItemSearched", 
			app:"Auzonet",
			objetive:900,
			pilottype:0,
			id:"kpi203"
		},		
		kpi202:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of items offered on loan, sell or donation. (Target=300)",
			type:"Objetive",
			retype:"ItemOffered", 
			app:"Auzonet",
			objetive:300,
			pilottype:0,
			id:"kpi202"
		},
		kpi201:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:" Number of registered users. (Target=50)",
			type:"Objetive",
			retype:"AppUserRegistered", 
			app:"Auzonet",
			objetive:50,
			pilottype:0,
			id:"kpi201"
		},		
		kpi200:{
			url:baseurl+"/analytics/apps/appkpi/count",
			title:"Number of times the app is started. (Target>600)",
			type:"Objetive",
			retype:"AppStarted", 
			app:"Auzonet",
			objetive:600,
			pilottype:0,
			id:"kpi200"
		},
		kpi119:{
			url:baseurl+"/analytics/common/appkpi/KPI5/pilot?pilot="+pilot[0],
			title:"Average app scores in "+pilot[0],
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi119"
		},
		kpi118:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4D/pilot?pilot="+pilot[0],
			title:"Would users recommend this app to a friend (10 max - 1 min) in "+pilot[0],
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi118"
		},
		kpi117:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4C/pilot?pilot="+pilot[0],
			title:"Happiness of users with the app design (5 max - 1 min) in "+pilot[0],
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi117"
		},
		kpi116:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4B/pilot?pilot="+pilot[0],
			title:"Usefulness of this app (5 max - 1 min) in "+pilot[0],
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi116"
		},
		kpi115:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4A/pilot?pilot="+pilot[0],
			title:"Easiness of use of this app (5 max - 1 min) in "+pilot[0],
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi115"
		},
		kpi114:{
			url:baseurl+"/analytics/common/appkpi/KPI3/pilot?pilot="+pilot[0],
			title:"Number of users who think this app increase the sense of community in "+pilot[0],
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi114"
		},
		kpi113:{
			url:baseurl+"/analytics/common/appkpi/KPI2/pilot?pilot="+pilot[0],
			title:"Number of users who completed the questionnaire and made improvement suggestions. Highly engaged users in "+pilot[0],
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi113"
		},
		kpi112:{
			url:baseurl+"/analytics/common/appkpi/KPI1.B/pilot?pilot="+pilot[0],
			title:"Number of users who would like to provide detailed feedback about the app. Highly engaged users in "+pilot[0],
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi112"
		},
		kpi111:{
			url:baseurl+"/analytics/common/appkpi/KPI1.A/pilot?pilot="+pilot[0],
			title:"Number of completed user surveys per app",
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi111"
		},		
		kpi109:{
			url:baseurl+"/analytics/common/appkpi/KPI5/count",
			title:"Average of App scores",
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi109"
		},
		kpi108:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4D/count",
			title:"Would users recommend this app to a friend (10 max - 1 min)",
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi108"
		},
		kpi107:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4C/count",
			title:"Happiness of users with the app design (5 max - 1 min)",
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi107"
		},
		kpi106:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4B/count",
			title:"Usefulness of this app (5 max - 1 min)",
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi106"
		},
		kpi105:{
			url:baseurl+"/analytics/common/appkpi2d/KPI4A/count",
			title:"Easiness of use of this app (5 max - 1 min)",
			type:"Avg",
			pilottype:0,
			color:"#ef5350",
			id:"kpi105"
		},
		kpi104:{
			url:baseurl+"/analytics/common/appkpi/KPI3/count",
			title:"Number of users who think this app increase the sense of community",
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi104"
		},
		kpi103:{
			url:baseurl+"/analytics/common/appkpi/KPI2/count",
			title:"Number of users who completed the questionnaire and made improvement suggestions. Highly engaged users",
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi103"
		},
		kpi102:{
			url:baseurl+"/analytics/common/appkpi/KPI1.B/count",
			title:"Number of users who would like to provide detailed feedback about the app. Highly engaged users",
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi102"
		},
		kpi101:{
			url:baseurl+"/analytics/common/appkpi/KPI1.A/count",
			title:"Number of completed user surveys per app",
			type:"Bar",
			pilottype:0,
			color:"#ef5350",
			id:"kpi101"
		},
		kpi50:{
			url:baseurl+"/analytics/controler/user/contact",
			title:"Users contact per month",
			type:"Line",
			pilottype:0,
			color:"#78909c",
			id:"kpi50"
		},		
		kpi49:{
			url:baseurl+"/analytics/controler/cities/usage",
			title:"Cities selection in WeLive",
			type:"Bar",
			pilottype:0,
			color:"#78909c",
			id:"kpi49"
		},		
		kpi48:{
			url:baseurl+"/analytics/controler/component/usage",
			title:"Usage of WeLive tools",
			type:"Bar",
			pilottype:0,
			color:"#78909c",
			id:"kpi48"
		},		
		kpi47:{
			url:baseurl+"/analytics/mkp/need/ranking",
			title:"Top 5 of WeLive Needs",
			type:"BarTop",
			pilottype:0,
			color:"#ffa726",
			id:"kpi47"
		},		
		kpi46:{
			url:baseurl+"/analytics/mkp/app/dataset/ranking",
			title:"Top 5 of WeLive Datasets",
			type:"BarTop",
			pilottype:0,
			color:"#ffa726",
			id:"kpi46"
		},		
		kpi45:{
			url:baseurl+"/analytics/mkp/app/buildingbock/ranking",
			title:"Top 5 of WeLive Building Blocks",
			type:"BarTop",
			pilottype:0,
			color:"#ffa726",
			id:"kpi45"
		},		
		kpi44:{
			url:baseurl+"/analytics/mkp/idea/ranking",
			title:"Top 5 of WeLive Ideas",
			type:"BarTop",
			pilottype:0,
			color:"#ffa726",
			id:"kpi44"
		},		
		kpi43:{
			url:baseurl+"/analytics/mkp/app/public/ranking",
			title:"Top 5 of WeLive Public Services",
			type:"BarTop",
			pilottype:0,
			color:"#ffa726",
			id:"kpi43"
		},
		kpi42dP:{
			url:baseurl+"/analytics/cdv/user/developer/all/count",
			title:"WeLive Users Developers",
			type:"BarOldPerc",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42dP"
		},
		
		kpi42d:{
			url:baseurl+"/analytics/cdv/user/developer/all/count",
			title:"WeLive Users Developers",
			type:"BarOld",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42d"
		},
		kpi42cP:{
			url:baseurl+"/analytics/cdv/user/role/all/count",
			title:"WeLive Users Role",
			type:"BarOldPerc",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42cP"
		},	
		
		kpi42c:{
			url:baseurl+"/analytics/cdv/user/role/all/count",
			title:"WeLive Users Role",
			type:"BarOld",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42c"
		},	
		kpi42bP:{
			url:baseurl+"/analytics/cdv/user/work/all/count",
			title:"WeLive Users Work",
			type:"BarOldPerc",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42bP"
		},	
		
		kpi42b:{
			url:baseurl+"/analytics/cdv/user/work/all/count",
			title:"WeLive Users Work",
			type:"BarOld",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42b"
		},	
		kpi42P:{
			url:baseurl+"/analytics/cdv/user/gender/all/count",
			title:"WeLive Users Gender",
			type:"BarOldPerc",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42P"
		},	
		
		kpi42:{
			url:baseurl+"/analytics/cdv/user/gender/all/count",
			title:"WeLive Users Gender",
			type:"BarOld",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi42"
		},	
		kpi41P:{
			url:baseurl+"/analytics/cdv/user/ages/all/count",
			title:"WeLive Users Age",
			type:"BarOldPerc",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi41P"
		},		
		
		kpi41:{
			url:baseurl+"/analytics/cdv/user/ages/all/count",
			title:"WeLive Users Age",
			type:"BarOld",
			pilottype:0,
			color:"#ab47bc",
			id:"kpi41"
		},		
		kpi35_1:{
			url:baseurl+"/analytics/mkp/app/open?pilot="+pilot[0],
			title:"Public Services Usage in city",
			type:"Bar",
			pilottype:0,
			color:"#8d6e63",
			id:"kpi35_1"
		},		
		kpi35:{
			url:baseurl+"/analytics/mkp/app/open",
			title:"Public Services Usage",
			type:"Bar",
			pilottype:0,
			color:"#8d6e63",
			id:"kpi35"
		},		
		kpi34:{
			url:baseurl+"/analytics/mkp/resource/download/count?pilot="+pilot[0],
			title:"Average downloanalytics per Public Service in city",
			type:"Bar",
			pilottype:0,
			color:"#8d6e63",
			id:"kpi34"
		},		
		kpi33:{
			url:baseurl+"/analytics/mkp/resource/download/count",
			title:"Average downloanalytics per Public Service",
			type:"Bar",
			pilottype:0,
			color:"#8d6e63",
			id:"kpi33"
		},		
		kpi27:{
			url:baseurl+"/analytics/cdv/user/all/count",
			title:"Registered Users",
			type:"Bar",
			pilottype:0,
			color:"#5064ef",
			id:"kpi27"
		},		
		kpi20b:{
			url:baseurl+"/analytics/mkp/buildingblock/delta",
			title:"Actual Building Blocks in WeLive",
			type:"Bar",
			pilottype:0,
			color:"#6eb0df",
			id:"kpi20b"
		},		
		kpi20:{
			url:baseurl+"/analytics/mkp/buildingblock/all",
			title:"Number of Building Blocks published in WeLive",
			type:"Bar",
			pilottype:0,
			color:"#6eb0df",
			id:"kpi20"
		},		
		kpi18:{
			url:baseurl+"/analytics/ods/dataset/usage/"+ccUserId+"/count",
			title:"My datasets usage",
			type:"Pie",
			pilottype:0,
			color:"#e44240",
			htmlid:"userDatasets",
			id:"kpi18"
		},
		kpi17:{
			url:baseurl+"/analytics/ods/dataset/usage/all/count?pilot="+pilot[0],
			title:"Top Datasets Accessed per city",
			type:"Bar",
			pilottype:0,
			color:"#e44240",
			id:"kpi17"
		},
		kpi16:{
			url:baseurl+"/analytics/ods/dataset/usage/all/count",
			title:"Top Datasets Accessed",
			type:"Bar",
			pilottype:0,
			color:"#e44240",
			id:"kpi16"
		},
		kpi15:{
			url:baseurl+"/analytics/ods/dataset/type/all/count",
			title:"Number of Datasets per city and type",
			type:"Bar2D",
			pilottype:0,
			color:"#e44240",
			id:"kpi15"
		},
		kpi14headerDATASETALL:{
			url:baseurl+"/analytics/oia/idea/search/all/materializedcitydelta",
			title:"Actual artefacts",
			type:"Bar2D",
			pilottype:0,
			color:"#bebf03",
			htmlid:"datasetPublished",
			id:"kpi14"
		},				
		kpi14headerDATASET:{
			//url:baseurl+"/analytics/ods/dataset/all/delta",
			url:baseurl+"/analytics/oia/idea/search/all/materializeddelta?pilot="+pilot[0],
			title:"Datasets published",
			type:"Number",
			htmlid:"datasetPublished",
			pilottype:0,
			id:"kpi14"
		},
		kpi14header:{
			url:baseurl+"/analytics/ods/dataset/all/delta",
			
			title:"Datasets published",
			type:"Number",
			htmlid:"datasetPublished",
			pilottype:0,
			id:"kpi14"
		},		
		/*kpi5:{
			url:baseurl+"/analytics/oia/idea/search/all/materialized?pilot="+pilot[0],
			title:"Artefacts published in city",
			type:"Number",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi5"
		},*/			
		kpi14act:{
			url:baseurl+"/analytics/ods/dataset/all/delta",
			title:"Number of Actual Datasets",
			type:"Bar",
			pilottype:0,
			color:"#e44240",
			id:"kpi14act"
		},
		kpi14:{
			url:baseurl+"/analytics/ods/dataset/all/count",
			title:"Number of Datasets published",
			type:"Bar",
			pilottype:0,
			color:"#e44240",
			id:"kpi14"
		},
		kpi13:{
			url:baseurl+"/analytics/oia/needpublished/"+ccUserId+"/count",//TODO no esta en el api-rest aun
			title:"My Published Needs",
			type:"Pie",
			pilottype:0,
			id:"kpi13"
		},			
		kpi12:{
			url:baseurl+"/analytics/oia/needpublished/"+ccUserId+"/count",
			title:"My Published Needs",
			type:"Pie",
			pilottype:0,
			htmlid:"userNeeds",
			color:"#bebf03", 
			id:"kpi12"
		},	
		kpi11header:{
			url:baseurl+"/analytics/oia/needpublished/delta",
			title:"Needs in Implementation Phase",
			type:"Number",
			htmlid:"needNumber",
			pilottype:0,
			id:"kpi11"
		},
		kpi11act:{
			url:baseurl+"/analytics/oia/needpublished/delta",
			title:"Number of Actual Needs",
			type:"Bar",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi11act"
		},		
		kpi11:{
			url:baseurl+"/analytics/oia/needpublished/count",
			title:"Number of Published Needs",
			type:"Bar",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi11"
		},		
		kpi10:{
			url:baseurl+"/analytics/oia/challengue/"+ccUserId+"/published",
			title:"My Published Challenges",
			type:"Line",
			pilottype:0,
			color:"#bebf03", 
			htmlid:"userChallenges",
			id:"kpi10"
		},		
		kpi9header:{
			url:baseurl+"/analytics/oia/challengue/delta",
			title:"Challenge in Implementation Phase",
			type:"Number",
			htmlid:"challengePublished",
			pilottype:0,
			id:"kpi9"
		},
		kpi9act:{
			url:baseurl+"/analytics/oia/challengue/delta",
			title:"Number of Published Challenges",
			type:"Bar",
			pilottype:0,
			
			color:"#bebf03", 
			id:"kpi9delta"
		},			
		kpi9:{
			url:baseurl+"/analytics/oia/challengue/published",
			title:"Number of Published Challenges",
			type:"Bar",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi9"
		},			
		kpi8:{
			url:baseurl+"/analytics/oia/idea/"+ccUserId+"/published",
			title:"My Published Ideas",
			type:"Pie",
			pilottype:0,
			color:"#bebf03", 
			htmlid:"userIdeas",
			id:"kpi8"
		},		
		kpi7header:{
			url:baseurl+"/analytics/oia/idea/delta",
			title:"Ideas in Implementation Phase",
			type:"Number",
			htmlid:"ideasPublished",
			pilottype:0,
			id:"kpi7"
		},
		kpi7act:{
			url:baseurl+"/analytics/oia/idea/delta",
			title:"Number of Actual Ideas",
			type:"Bar",
			pilottype:0,
			htmlid:"userArtefacts",
			color:"#bebf03", 
			id:"kpi7act"
		},		
		kpi7:{
			url:baseurl+"/analytics/oia/idea/published",
			title:"Number of Published Ideas",
			type:"Bar",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi7"
		},		
		kpi6:{
			url:baseurl+"/analytics/oia/idea/search/"+ccUserId+"/materializedcity",
			title:"My Artefacts",
			type:"Bar2D",
			pilottype:0,
			color:"#935a32", 
			id:"kpi6"
		},		
		kpi5act:{
			url:baseurl+"/analytics/oia/idea/search/all/materializeddelta?pilot="+pilot[0],
			title:"Actual artefacts in city",
			type:"Number",
			pilottype:0,
			color:"#935a32", 
			id:"kpi5act"
		},			
		kpi4act:{
			url:baseurl+"/analytics/oia/idea/search/all/materializedcitydelta",
			title:"Actual artefacts",
			type:"Bar2D",
			pilottype:0,
			color:"#935a32", 
			id:"kpi4act"
		},			
		kpi5:{
			url:baseurl+"/analytics/oia/idea/search/all/materialized?pilot="+pilot[0],
			title:"Artefacts published in city",
			type:"Number",
			pilottype:0,
			color:"#935a32", 
			id:"kpi5"
		},			
		kpi4:{
			url:baseurl+"/analytics/oia/idea/search/all/materializedcity",
			title:"Artefacts published",
			type:"Bar2D",
			pilottype:0,
			color:"#935a32", 
			id:"kpi4"
		},		
		kpi3:{
			url:baseurl+"/analytics/oia/ideainimplementation/"+ccUserId+"/count",
			title:"My ideas in Implementation Phase",
			type:"Pie",
			pilottype:0,
			color:"#bebf03", 
			htmlid:"userIdeasImpl",
			id:"kpi3"
		},
		kpi2header:{
			url:baseurl+"/analytics/oia/ideainimplementation/count",
			title:"Ideas in Implementation Phase",
			type:"Number",
			htmlid:"N/A",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi2"
		},
		kpi2:{
			url:baseurl+"/analytics/oia/ideainimplementation/count",
			title:"Ideas in Implementation Phase",
			type:"Bar",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi2"
		},
		kpi1header:{
			url:baseurl+"/analytics/cdv/user/all/count",
			title:"Registered Users",
			type:"Number",
			pilottype:0,
			htmlid:"registeredUsers",
			id:"kpi1"
		},
		kpi1:{
			url:baseurl+"/analytics/cdv/user/all/count",
			title:"Registered Users",
			type:"Bar",
			pilottype:0,
			color:"#bebf03", 
			id:"kpi1"
		}
		
}




var fakedata = {
	    labels: ["January", "February", "March", "April", "May", "June", "July"],
	    datasets: [
	        {
	            label: "bilbao",
	            fillColor: "rgba(220,220,220,0.5)",
	            strokeColor: "rgba(220,220,220,0.8)",
	            highlightFill: "rgba(220,220,220,0.75)",
	            highlightStroke: "rgba(220,220,220,1)",
	            data: [65, 59, 80, 81, 56, 55, 40]
	        },
	        {
	            label: "trento",
	            fillColor: "rgba(151,187,205,0.5)",
	            strokeColor: "rgba(151,187,205,0.8)",
	            highlightFill: "rgba(151,187,205,0.75)",
	            highlightStroke: "rgba(151,187,205,1)",
	            data: [28, 48, 40, 19, 86, 27, 90]
	        }
	    ]
	};


var fakedata1 = {
	    labels: ["Bilbon", "Bilbozkatu", "Auzonet"],
	    datasets: [
	        {
	            label: "AppStarted",
	            fillColor: "rgba(181, 189,0,0.5)",
	            strokeColor: "rgba(181, 189,0,0.8)",
	            highlightFill: "rgba(181, 189,0,0.75)",
	            highlightStroke: "rgba(181, 189,0,1)",
	            data: [200, 350, 275]
	        },
	        {
	            label: "Objetive",
	            fillColor: "rgba(239, 51, 64,0.5)",
	            strokeColor: "rgba(239, 51, 64,0.8)",
	            highlightFill: "rgba(239, 51, 64,0.75)",
	            highlightStroke: "rgba(239, 51, 64,1)",
	            data: [600, 500, 600]
	        }
	    ]
	};
var fakedataline = {
	    labels: ["January", "February", "March", "April", "May", "June", "July"],
	    datasets: [
	        {
	            label: "My First dataset",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: [65, 59, 80, 81, 56, 55, 40]
	        },
	        {
	            label: "My Second dataset",
	            fillColor: "rgba(151,187,205,0.2)",
	            strokeColor: "rgba(151,187,205,1)",
	            pointColor: "rgba(151,187,205,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(151,187,205,1)",
	            data: [28, 48, 40, 19, 86, 27, 90]
	        }
	    ]
	};	
var fakedatapie = [
            {
                value: 300,
                color:"#F7464A",
                highlight: "#FF5A5E",
                label: "Red"
            },
            {
                value: 50,
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "Green"
            },
            {
                value: 100,
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "Yellow"
            }
        ]

var faketable=''+
'	<div class="tablaDatos">'+
'	<div class="lineaDato">'+
'		<div class="valorDato">30</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">40</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">50</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">60</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">60</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">60</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">60</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">60</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'	<div class="lineaDato">'+
'		<div class="valorDato">60</div>'+
'		<div class="tituloDato">January</div>'+
'	</div>'+
'</div>';

var fakevalues=[["","","",""],[123,45,32,7]];
