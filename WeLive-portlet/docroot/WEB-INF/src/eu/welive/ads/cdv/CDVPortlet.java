package eu.welive.ads.cdv;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CDVPortlet extends MVCPortlet {
	public static final String KEY_PILOT = "LIFERAY_SHARED_pilot";
	private static final Log _log = LogFactoryUtil.getLog(CDVPortlet.class.getName());
	public CDVPortlet() {
	}
	public void changePref(ActionRequest req,ActionResponse res) throws IOException,PortletException, SystemException
	{
	      String kpi11=req.getParameter("kpi11");
	      String kpi43=req.getParameter("kpi43");
	      String kpi11_1=req.getParameter("kpi11_1");
	      String kpi11_2=req.getParameter("kpi11_2");
	      if (kpi11 == null) kpi11 = "false";
	      else if (kpi11.equals("on")) kpi11 = "true";
	      
	      if (kpi43 == null) kpi43 = "false";
	      else if (kpi43.equals("on")) kpi43 = "true";
	      
	      if (kpi11_1 == null) kpi11_1 = "false";
	      else if (kpi11_1.equals("on")) kpi11_1 = "true";
	      
	      if (kpi11_2 == null) kpi11_2 = "false";
	      else if (kpi11_2.equals("on")) kpi11_2 = "true";
	      //getting portlet resource
		      //String portletResource=ParamUtil.getString(req,"PortletResource");
	      PortletPreferences pre=req.getPreferences();
	      pre.setValue("kpi11", kpi11);
	      pre.setValue("kpi43", kpi43);
	      pre.setValue("kpi11_1", kpi11_1);
	      pre.setValue("kpi11_2", kpi11_2);
	      pre.store();
	      //String str=pre.getValue("deptname","");
	      //System.out.println(str);
	     
	}
	public void selectInApp(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		//String pilot = ParamUtil.getString(actionRequest, "pilot", null);
		
		PortletSession session = actionRequest.getPortletSession();
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		String access_token = ParamUtil.getString(actionRequest, "access_token", null);//request.getParameter("access_token");	
		String expires_in =  ParamUtil.getString(actionRequest, "expires_in", null);//request.getParameter("expires_in");	
		//actionResponse.sendRedirect("/web/welive/ads");
		actionResponse.setRenderParameter("mvcPath", "/html/ads/inApp.jsp?access_token="+access_token+"&expires_in="+expires_in);
		//actionResponse.actionResponse.sendRedirect("/challenges_explorer");
	}	

}
