package eu.welive.ads.client;

import java.util.Hashtable;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Configuration;
import javax.net.ssl.*;

import org.glassfish.jersey.client.ClientConfig;






import com.liferay.util.portlet.PortletProps;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class RestClient {
	
	private String _baseUrl ="";
	private String user ="";
	private String pass =""; 
	
	private final static Logger logger = Logger.getLogger(RestClient.class.getName());
	
	public RestClient() {
		_baseUrl = PortletProps.get("BASE_URL");
		//user= "welive";
		//pass ="w3l1v3t00ls";
		 //logger.info("_baseUrl: "+_baseUrl);
		 System.out.println("_baseUrl: "+_baseUrl);
	}
	public RestClient(String user, String pass) {
		_baseUrl = PortletProps.get("BASE_URL");
		this.user= user;
		this.pass =pass;
		 //logger.info("_baseUrl: "+_baseUrl);
		 System.out.println("_baseUrl: "+_baseUrl);
	}
	
	public Client initClient(Configuration config)  {
		SSLContext ctx;
		try {
			ctx = SSLContext.getInstance("SSL");
	        
			ctx.init(null, certs, new SecureRandom());

		    return ClientBuilder.newBuilder()
		                .withConfig(config)
		                .hostnameVerifier(new TrustAllHostNameVerifier())
		                .sslContext(ctx)
		                .build();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return null;
	    
	}

	TrustManager[] certs = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
            }
    };

    public static class TrustAllHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }	

	private String Post(String sUrl, Hashtable<String, ?> hData) {
		
		
		String aux="";
		if (hData!=null) aux=hData.toString();
		//logger.info("Post: "+sUrl+" Data: "+aux);

		return Post(sUrl,  aux);
		
		
	}
	private String Post(String sUrl, String hData) {
		
		Client client = null;
		//logger.info("Post: "+sUrl+" Data: "+aux);
		System.out.println("Post: "+sUrl+" Data: "+hData);
		if (_baseUrl.startsWith("https")){
			client =initClient(new ClientConfig());				 
		}
		else{
			client = ClientBuilder.newClient();
		}
		
		if (user!=null && !user.equals("") && pass!=null && !pass.equals("")){
			client.register(new Authenticator(user, pass));				
		}
		WebTarget target = client.target(_baseUrl);
		Response r = target.path(sUrl).request(MediaType.APPLICATION_JSON_TYPE).post(
				Entity.entity(hData, MediaType.APPLICATION_JSON_TYPE));


		//System.out.println(r.getStatus());
		return r.readEntity(String.class);
		
	}	
	private String Get(String sUrl){
		Client client = null;
		logger.info("Get: "+sUrl);
		System.out.println("Get: "+sUrl);
		if (_baseUrl.startsWith("https")){
			client =initClient(new ClientConfig());				 
		}
		else{
			client = ClientBuilder.newClient();
		}
		
		if (user!=null && !user.equals("") && pass!=null && !pass.equals("")){
			client.register(new Authenticator(user, pass));				
		}
		WebTarget target = client.target(sUrl);
		Response r = target.request(MediaType.APPLICATION_JSON_TYPE).get();

		String respuesta = r.readEntity(String.class);
		int sttus =r.getStatus();
		System.out.println("Status: "+sttus+" respuesta:"+respuesta);
		if (sttus != 200){
			return "";
		}
		return respuesta;	
		
	}
	public String getAuthToken(String grant_type,String client_id, String client_secret){
		//_baseUrl = RestClientDefs.BASE_URL;
		String respuesta = Get(_baseUrl+RestClientDefs.TOKEN+"?grant_type="+grant_type+"&client_id="+client_id+"&client_secret="+client_secret);
		System.out.println("getAuthToken:"+respuesta);
		return respuesta;
	}

	public String getKPI1_1(){
		//_baseUrl = RestClientDefs.BASE_URL;
		String respuesta = Get(_baseUrl+RestClientDefs.KPI1_1);
		System.out.println("getKPI1_1:"+respuesta);
		return respuesta;
	}
	public String getKPI4_3(){
		String respuesta =  Get(_baseUrl+RestClientDefs.KPI4_3);
		System.out.println("getKPI4_3:"+respuesta);
		return respuesta;
		
	}
	public String getKPI2_1(){
		//_baseUrl = RestClientDefs.BASE_URL;
		String respuesta = Get(_baseUrl+RestClientDefs.KPI2_1);
		System.out.println("getKPI2_1:"+respuesta);
		return respuesta;
	}
	public String getKPI2_2(){
		//_baseUrl = RestClientDefs.BASE_URL;
		String respuesta = Get(_baseUrl+RestClientDefs.KPI2_2);
		System.out.println("getKPI2_2:"+respuesta);
		return respuesta;
	}	
	public String getKPI2_3(){
		//_baseUrl = RestClientDefs.BASE_URL;
		String respuesta = Get(_baseUrl+RestClientDefs.KPI2_3);
		System.out.println("getKPI2_3:"+respuesta);
		return respuesta;
	}	
	public String getKPI2_3_1(String userid){
		//_baseUrl = RestClientDefs.BASE_URL;
		String respuesta = Get(_baseUrl+RestClientDefs.KPI2_3_1+"/"+userid+"/count");
		System.out.println("getKPI2_3_1:"+respuesta);
		return respuesta;
	}		
	public String getKPI11_1(){
		String respuesta = Get(_baseUrl+RestClientDefs.KPI11_1);
		System.out.println("getKPI11_1:"+respuesta);
		return respuesta;
		
	}
	public String getKPI11_2(){
		String respuesta =Get(_baseUrl+RestClientDefs.KPI11_2);
		System.out.println("getKPI11_2:"+respuesta);
		return respuesta;
		//return 
	}	
}

