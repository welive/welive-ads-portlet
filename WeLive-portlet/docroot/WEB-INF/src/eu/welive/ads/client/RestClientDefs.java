package eu.welive.ads.client;

public class RestClientDefs {

	// URL definitions.	
	//public static final String BASE_URL = "http://localhost:8080";///ads/weliveapi/ads/cdv/stakeholders
	public static final String KPI1_1 = "/dev/api/ads/cdv/stakeholders";//{pilotID}";
	public static final String KPI4_3 = "/dev/api/ads/cdv/user/all/count";
	public static final String KPI11_1 = "/dev/api/ads/cdv/user/ages/all/count";
	public static final String KPI11_2 = "/dev/api/ads/cdv/user/gender/all/count";
	public static final String KPI2_1 = "/dev/api/ads/ods/dataset/all/count";//https://dev.welive.eu/dev/api/ads/ods/dataset/all/count
	public static final String KPI2_2 = "/dev/api/ads/ods/dataset/type/all/count";
	public static final String KPI2_3 = "/dev/api/ads/ods/dataset/usage/all/count";
	public static final String KPI2_3_1 = "/dev/api/ads/ods/dataset/usage";
	public static final String TOKEN = "/aac/oauth/token";
	
}
