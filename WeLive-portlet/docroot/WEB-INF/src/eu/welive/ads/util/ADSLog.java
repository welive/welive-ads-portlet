package eu.welive.ads.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
public class ADSLog {

	
	 private final static Logger logger = Logger.getLogger(ADSLog.class.getName());
	 private static FileHandler fh = null;
	 
	 public static void init(){
		 try {
			 fh=new FileHandler("ADS.log", false);
		 } 
		 catch (SecurityException se) {
			 se.printStackTrace();
		 }
		 catch (IOException ioe) {
			 ioe.printStackTrace();
		 }
		 Logger l = Logger.getLogger("");
		 fh.setFormatter(new SimpleFormatter());
		 l.addHandler(fh);
		 l.setLevel(Level.CONFIG);
	 }
}
