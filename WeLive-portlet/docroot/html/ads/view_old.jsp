<%
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.model.Role"%>

<portlet:defineObjects />
<theme:defineObjects />

<%
	Long ccUserId = 193L;
	String pilot = "['Trento', 'Trento', 'trento', 'Trento']";
	pilot = "['Bilbao','Bilbao', 'bilbao', 'Bilbao City Council']";
	String role = "ROLE-";//City Academic Bussines


try{
	//expanded attributes: ("CCUserID", "pilot", "isDeveloper", "address", "city", "zipCode", "country" y "languages"):
	ccUserId = (Long)user.getExpandoBridge().getAttribute("CCUserID");
    String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
    if(pilotArray != null && pilotArray.length > 0) {
        pilot = pilotArray[0];
    }
    
    if(themeDisplay.isSignedIn() && user != null) {
        List<Role> roleList = user.getRoles();
        for(Role rol : roleList) {
        	role+="-"+rol.getName();
        }
    }
    
    if(themeDisplay.getPermissionChecker().isOmniadmin()){
    	role+="-OnmiAdmin";
    }
	
	if (pilot.equals("Bilbao")){
		pilot = "['Bilbao','Bilbao', 'bilbao', 'Bilbao City Council']";
	}
	
	if (pilot.equals("Trento")){
		pilot = "['Trento', 'Trento', 'trento', 'Trento']";
	}
	
	if (pilot.equals("Novisad")){
		pilot = "['Novisad','Novi Sad', 'novisad', 'Novisad']";
	}
	
	if (pilot.equals("Uusimaa")){
		pilot = "['Uusimaa', 'Region on Uusimaa-Helsinki', 'helsinki', 'Helsinki']";
	}
	
	} catch (Exception e) {
%>
		<h1><%=e.getMessage() %></h1>
<%

	}
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script>
		var pilot=<%=pilot %>;
		var ccUserId=<%=ccUserId %>;
		var role="<%=role %>";
	</script>

</head>
<body>
User Id: <%=ccUserId %> Pilot:  <%=pilot %> Role: <%=role %> 

	<div class="fixedBodyDiv">
		<div class="cabecera">
			<span>WeLive ADS</span>
			<div class="iconMenuDrc"></div>
			<div id="dates" class="datebox">
				<select style="display:none" onchange="seleccionarPerfil(this.value);">
					<option value=0>--Todos--</option>
					<option value=1>Super Usuario</option>
					<option value=2>Ciudad</option>
					<option value=3>Ciudadano</option>
				</select>
				<div class="pickerContainer" style="right:320px">	
        			<input class="pickerInput" placeholder="Start Date..." type="text" id="fromDate" onblur="hidePicker('fromDateMonth')" onfocus="showPicker('fromDateMonth', this)">
        			<div id ="fromDateMonth"></div>
        		</div>
				<div class="pickerContainer" style="right:70px">	
        			<input class="pickerInput" placeholder="End Date..." type="text" id="toDate" onblur="hidePicker('toDateMonth')"  onfocus="showPicker('toDateMonth', this)">
        			<div id ="toDateMonth"></div>
        		</div>
				
				<!--  input id="fromDate" type="date" placeholder="Start Date...">
				<input id="toDate"  type="date" placeholder="End Date..."-->
			</div>
		</div>
		<div class="cuerpo">
			<div class="menulateral">
				<div class="titSeccion">Platform KPIs</div>
				<div class="seccionMenu">
					<div class="titMenu" onclick="changeFatherOverflow(this);">Open Innovation Area</div>
					<div class="opcionmenu userciudad usersuper" title="Registered Users" onclick="create1DBarChart(kpidata.kpi1);">(KPI1.1)Registered Users</div>
					<div class="opcionmenu userciudad usersuper" title="Ideas in Implementation Phase" onclick="create1DBarChart(kpidata.kpi2);">(KPI1.2)Ideas in Implementation Phase</div>
					<div class="opcionmenu userciudadano" title="My ideas in Implementation Phase" onclick="createPieChart(kpidata.kpi3);">(KPI1.2.1))My ideas in Implementation Phase</div>
					<div class="opcionmenu usersuper" title="Artefacts published" onclick="create2DBarChart(kpidata.kpi4);">(KPI1.3)Artefacts published</div>
					<div class="opcionmenu userciudad" title="Artefacts published in City" onclick="createNumberChart(kpidata.kpi5);">(KPI1.3)Artefacts published in City</div>
<!--  				
					<div class="opcionmenu usersuper" title="Artefacts removed" onclick="create2DBarChart(kpidata.kpi4_1);">(KPI1.3)Artefacts removed</div>
					<div class="opcionmenu userciudad" title="Artefacts removed in City" onclick="createNumberChart(kpidata.kpi5_1);">(KPI1.3)Artefacts removed in City</div>
-->
					<div class="opcionmenu usersuper" title="Actual artefacts" onclick="create2DBarChart(kpidata.kpi4act);">(KPI1.3)Actual artefacts</div>
					<div class="opcionmenu userciudad" title="Actual artefacts in City" onclick="createNumberChart(kpidata.kpi5act);">(KPI1.3)Actual artefacts in City</div>
					<div class="opcionmenu userciudadano" title="My Artefacts" onclick="create2DBarChart(kpidata.kpi6);">(KPI1.3.1)My Artefacts</div>
					<div class="opcionmenu userciudad usersuper" title="Published Ideas" onclick="create1DBarChart(kpidata.kpi7);">(KPI1.4)Published Ideas</div>
					<div class="opcionmenu userciudad usersuper" title="Actual Ideas" onclick="create1DBarChart(kpidata.kpi7act);">(KPI1.4)Actual Ideas</div>
					<div class="opcionmenu userciudadano" title="My Published Ideas" onclick="createPieChart(kpidata.kpi8);">(KPI1.4.1)My Published Ideas</div>
					<div class="opcionmenu userciudad usersuper" title="Number of Challenges" onclick="create1DBarChart(kpidata.kpi9);">(KPI1.5)Number of Challenges</div>
					<div class="opcionmenu userciudad usersuper" title="Actual Challenges" onclick="create1DBarChart(kpidata.kpi9act);">(KPI1.5)Actual Challenges</div>
					<div class="opcionmenu userciudadano" title="Challenges where I participated" onclick="createPieChart(kpidata.kpi10);">(KPI1.5.1)Challenges where I participated</div>
					<div class="opcionmenu userciudad usersuper" title="Number of Needs" onclick="create1DBarChart(kpidata.kpi11);">(KPI1.7)Number of Needs</div>
					<div class="opcionmenu userciudad usersuper" title="Actual Needs" onclick="create1DBarChart(kpidata.kpi11act);">(KPI1.7)Actual Needs</div>
					<div class="opcionmenu userciudadano" title="My Needs" onclick="createPieChart(kpidata.kpi12);">(KPI1.7.1)My Needs</div>
					<!-- <div class="opcionmenu" onclick="createPieChart(kpidata.kpi13);">(KPI1.7.2)Needs where I collaborate</div>-->
				</div>
				<div class="seccionMenu">
					<div class="titMenu" onclick="changeFatherOverflow(this);">Open Data Toolset</div>
				<div class="opcionmenu  userciudad usersuper" title="Datasets published" onclick="create1DBarChart(kpidata.kpi14);">(KPI2.1)Datasets published</div>
				<div class="opcionmenu  userciudad usersuper" title="Actual Datasets" onclick="create1DBarChart(kpidata.kpi14act);">(KPI2.1)Actual Datasets</div>
				<div class="opcionmenu  userciudad usersuper" title="Number of Datasets per city and type" onclick="create2DBarChart(kpidata.kpi15);">(KPI2.2)Number of Datasets by type</div>
				<div class="opcionmenu  usersuper" title="Top Datasets Accessed" onclick="create1DBarChartTop(kpidata.kpi16, 10);">(KPI2.3)Top Datasets Accessed</div>
				<div class="opcionmenu  userciudad" title="Top Datasets Accessed per City" onclick="create1DBarChartTop(kpidata.kpi17, 10);">(KPI2.3)Top Datasets Accessed per city</div>
				<div class="opcionmenu userciudadano" title="My datasets usage" onclick="createPieChart(kpidata.kpi18);">(KPI2.3.1)My datasets usage</div>
				</div>
				<div class="seccionMenu userciudad usersuper">
					<div class="titMenu" onclick="changeFatherOverflow(this);">Open Services Framework</div>
				<div class="opcionmenu" title="Number of Building Blocks published in WeLive" onclick="create1DBarChart(kpidata.kpi20);">(KPI3.1)Number of Building Blocks published in WeLive</div>
				</div>
				<div class="seccionMenu userciudad usersuper">
					<div class="titMenu" onclick="changeFatherOverflow(this);">Personalization of Public Services</div>
				<div class="opcionmenu" title="Registered Users"  onclick="create1DBarChart(kpidata.kpi27);">(KPI4.3)Registered Users</div>
				</div>
				<div class="seccionMenu userciudad usersuper">
					<div class="titMenu" onclick="changeFatherOverflow(this);">Stakeholder Behaviour Change</div>
				<div class="opcionmenu  usersuper" title="Average downloads per Public Service" onclick="create1DBarChart(kpidata.kpi33);">(KPI5.3)Average downloads per Public Service</div>
				<div class="opcionmenu  userciudad" title="Average downloads per Public Service in my city" onclick="create1DBarChart(kpidata.kpi34);">(KPI5.3)Average downloads per Public Service in my city</div>
				<div class="opcionmenu usersuper" title="Public Service Usage" onclick="create1DBarChart(kpidata.kpi35);">(KPI5.4)Public Service Usage</div>
				<div class="opcionmenu  userciudad" title="Public Service Usage in my city" onclick="create1DBarChart(kpidata.kpi35_1);">(KPI5.4)Public Service Usage in my city</div>
				</div>
				<div class="seccionMenu  userciudad usersuper">
					<div class="titMenu" onclick="changeFatherOverflow(this);">User Age/Gendre</div>
				<div class="opcionmenu" title="WeLive Users Age"  onclick="create2DBarChartOld(kpidata.kpi41);">(KPI11.1)WeLive Users Age</div>
				<div class="opcionmenu" title="WeLive Users gender"  onclick="create2DBarChartOld(kpidata.kpi42);">(KPI11.2)WeLive Users gender</div>
				</div>
				<div class="seccionMenu userciudad usersuper">
					<div class="titMenu" onclick="changeFatherOverflow(this);">Rankings</div>
				<div class="opcionmenu" title="Top 5 of WeLive Public Services" onclick="create1DBarChartTop(kpidata.kpi43);">(KPI12.1)Top 5 of WeLive Public Services</div>
				<div class="opcionmenu" title="Top 5 of WeLive Ideas" onclick="create1DBarChartTop(kpidata.kpi44);">(KPI12.2)Top 5 of WeLive Ideas</div>
				<div class="opcionmenu" title="Top 5 of Building Blocks" onclick="create1DBarChartTop(kpidata.kpi45);">(KPI12.3)Top 5 of WeLive Building Block</div>
				<div class="opcionmenu" title="Top 5 of Datasets" onclick="create1DBarChartTop(kpidata.kpi46);">(KPI12.4)Top 5 of WeLive Datasets</div>
				<div class="opcionmenu" title="Top 5 of Needs" onclick="create1DBarChartTop(kpidata.kpi47);">(KPI12.6)Top 5 of WeLive Needs</div>
				</div>
				<div class="seccionMenu  userciudad usersuper">
					<div class="titMenu" onclick="changeFatherOverflow(this);"> WeLive framework</div>
				<div class="opcionmenu  userciudad usersuper" title="Usage of WeLive tools" onclick="create1DBarChart(kpidata.kpi48);">(KPI13.1)Usage of WeLive tools</div>
				<div class="opcionmenu  userciudad usersuper" title="Cities selection in WeLive" onclick="create1DBarChart(kpidata.kpi49);">(KPI13.2)Cities selection in WeLive</div>
				<div class="opcionmenu  usersuper" title="Users contact per month" onclick="create1DLineChartMonth(kpidata.kpi50);">(KPI13.3)Users contact per month</div>
				</div>
				<!--  
				<div class="opcionmenu" onclick="createChart('kpi19', 'Pie', this.innerHTML);">Datasets per quality</div>
				<div class="opcionmenu" onclick="createChart('kpi20', 'Doughnut', this.innerHTML);">Building Blocks published in WeLive</div>
				<div class="opcionmenu" onclick="createChart('kpi21', 'Doughnut', this.innerHTML);"> Building blocks exchanged - Reusage of Building Blocks</div>
				<div class="opcionmenu" onclick="createChart('kpi22', 'Doughnut', this.innerHTML);">Building Blocks per service</div>
				<div class="opcionmenu" onclick="createChart('kpi23', 'Doughnut', this.innerHTML);">Public Services published in WeLive</div>
				<div class="opcionmenu" onclick="createChart('kpi24', 'Doughnut', this.innerHTML);">Services per month and type</div>
				<div class="opcionmenu" onclick="createChart('kpi25', 'Doughnut', this.innerHTML);">Public Services promoting Transparency</div>
				<div class="opcionmenu" onclick="createChart('kpi26', 'Doughnut', this.innerHTML);">Personalizable Public Services</div>
				<div class="opcionmenu" onclick="createChart('kpi27', 'Doughnut', this.innerHTML);">Registered Users inCDV</div>
				<div class="opcionmenu" onclick="createChart('kpi28', 'Doughnut', this.innerHTML);">Active Users of the Open Innovation Area</div>
				<div class="opcionmenu" onclick="createChart('kpi29', 'Doughnut', this.innerHTML);">Active Users per public service</div>
				<div class="opcionmenu" onclick="createChart('kpi30', 'Doughnut', this.innerHTML);">Developers registered in the WeLive ecosystem</div>
				<div class="opcionmenu" onclick="createChart('kpi31', 'Doughnut', this.innerHTML);">Number of Collaborators</div>
				<div class="opcionmenu" onclick="createChart('kpi32', 'Doughnut', this.innerHTML);">Number of Consumers</div>
				<div class="opcionmenu" onclick="createChart('kpi36', 'Doughnut', this.innerHTML);">Building Blocks Usage </div>
				<div class="opcionmenu" onclick="createChart('kpi37', 'Doughnut', this.innerHTML);">Searches performed and recommendations received per month</div>
				<div class="opcionmenu" onclick="createChart('kpi38', 'Doughnut', this.innerHTML);">Number of Companies creating public services per month</div>
				<div class="opcionmenu" onclick="createChart('kpi39', 'Doughnut', this.innerHTML);">Evolution of Datasets, building blocks and Public Services.</div>
				<div class="opcionmenu" onclick="createChart('kpi40', 'Doughnut', this.innerHTML);">Evolution of access to  WeLive ecosystem</div>
				<div class="opcionmenu" onclick="createChart('kpi51', 'Doughnut', this.innerHTML);">Access to API documentation per month</div>
				<div class="opcionmenu" onclick="createChart('kpi52', 'Doughnut', this.innerHTML);">Usage of BBs hosted in CNS and cloudfoundry</div>
				-->
				<div class="usersuper userciudad">
					<div class="titSeccion">Application KPIs</div>
					<div class="seccionMenu">
						<div class="titMenu" onclick="changeFatherOverflow(this);">Common Apps KPIs</div>
						<div class="usersuper">
							<div class="opcionmenu" title="Number of completed user surveys per app" onclick="create1DBarChart(kpidata.kpi101);">(KPI1.A)Number of completed user surveys per app</div>
							<div class="opcionmenu" title="Number of users who would like to provide detailed feedback about the app. Highly engaged users" onclick="create1DBarChart(kpidata.kpi102);">(KPI1.B)Number of users who would like to provide detailed feedback about the app. Highly engaged users</div>
							<div class="opcionmenu" title="Number of users who completed the questionnaire and made improvement suggestions. Highly Engageed users" onclick="create1DBarChart(kpidata.kpi103);">(KPI2)Number of users who completed the questionnaire and made improvement suggestions. Highly Engageed users</div>
							<div class="opcionmenu" title="Number of users who think this app increase the sense of community" onclick="create1DBarChart(kpidata.kpi104);">(KPI3)Number of users who think this app increase the sense of community</div>
							<div class="opcionmenu" title="Users who think this app is easy to use(5 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi105);">(KPI4A)Users who think this app is easy to use(5 max - 1 min)</div>
							<div class="opcionmenu" title="Users who consider this app is useful(5 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi106);">(KPI4B)Users who consider this app is useful(5 max - 1 min)</div>
							<div class="opcionmenu" title="Users who are happy with the app design(5 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi107);">(KPI4C)Users who are happy with the app design(5 max - 1 min)</div>
							<div class="opcionmenu" title="Users who would recommend this app to a friend(10 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi108);">(KPI4D)Users who would recommend this app to a friend(10 max - 1 min)</div>
							<div class="opcionmenu" title="Number of app scores over 3,5" onclick="create1DBarChart(kpidata.kpi109);">(KPI5)Number of app scores over 3,5</div>
						</div>
						<div class="userciudad">
							<div class="opcionmenu" title="Number of completed user surveys per app" onclick="create1DBarChart(kpidata.kpi111);">(KPI1.A Pilot)Number of completed user surveys per app</div>
							<div class="opcionmenu" title="Number of users who would like to provide detailed feedback about the app. Highly engaged users" onclick="create1DBarChart(kpidata.kpi112);">(KPI1.B Pilot)Number of users who would like to provide detailed feedback about the app. Highly engaged users</div>
							<div class="opcionmenu" title="Number of users who completed the questionnaire and made improvement suggestions. Highly Engageed users" onclick="create1DBarChart(kpidata.kpi113);">(KPI2 Pilot)Number of users who completed the questionnaire and made improvement suggestions. Highly Engageed users</div>
							<div class="opcionmenu" title="Number of users who think this app increase the sense of community" onclick="create1DBarChart(kpidata.kpi114);">(KPI3 Pilot)Number of users who think this app increase the sense of community</div>
							<div class="opcionmenu" title="Users who think this app is easy to use(5 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi115);">(KPI4A)Users who think this app is easy to use(5 max - 1 min)</div>
							<div class="opcionmenu" title="Users who consider this app is useful(5 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi116);">(KPI4B)Users who consider this app is useful(5 max - 1 min)</div>
							<div class="opcionmenu" title="Users who are happy with the app design(5 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi117);">(KPI4C)Users who are happy with the app design(5 max - 1 min)</div>
							<div class="opcionmenu" title="Users who would recommend this app to a friend(10 max - 1 min)" onclick="createAVGBarChart(kpidata.kpi118);">(KPI4D)Users who would recommend this app to a friend(10 max - 1 min)</div>
							<div class="opcionmenu" title="Number of app scores over 3,5" onclick="create1DBarChart(kpidata.kpi119);">(KPI5 Pilot)Number of app scores over 3,5</div>
						</div>
					</div>
					<div class="userbilbao">
					<div class="titSeccion">Bilbao KPIs</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Bilbozkatu KPIs</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi210);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi211);">Number of active users</div>
							<div class="opcionmenu" title="Number of new voting campaigns organised" onclick="createObjetiveChart(kpidata.kpi212);">Number of new voting campaigns organised</div>
							<div class="opcionmenu" title="Number of votes per campaign received" onclick="createObjetiveChart(kpidata.kpi213);">Number of votes per campaign received</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Bilbon KPIs</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi220);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi221);">Number of active users</div>
							<div class="opcionmenu" title="Number of new POIs added " onclick="createObjetiveChart(kpidata.kpi222);">Number of new POIs added </div>
							<div class="opcionmenu" title="Number of searched POIs" onclick="createObjetiveChart(kpidata.kpi223);">Number of searched POIs</div>
							<div class="opcionmenu" title="Number of selected POIs to view details" onclick="createObjetiveChart(kpidata.kpi224);">Number of selected POIs to view details</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Auzonet KPIs</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi200);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi201);">Number of active users</div>
							<div class="opcionmenu" title="Number of items offered on loan, sell or donation" onclick="createObjetiveChart(kpidata.kpi202);">Number of items offered on loan, sell or donation</div>
							<div class="opcionmenu" title="Number of searches for items of interest" onclick="createObjetiveChart(kpidata.kpi203);">Number of searches for items of interest</div>
							<div class="opcionmenu" title="Number of borrowed/bought products" onclick="createObjetiveChart(kpidata.kpi204);">Number of borrowed/bought products</div>
							<div class="opcionmenu" title="Number of items requested on loan, sell or donation" onclick="createObjetiveChart(kpidata.kpi205);">Number of items requested on loan, sell or donation</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Faborenet KPIs</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi230);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi231);">Number of active users</div>
							<div class="opcionmenu" title="Number of user profile skills and service offers registered" onclick="createObjetiveChart(kpidata.kpi232);">Number of user profile skills and service offers registered</div>
							<div class="opcionmenu" title="Number of searches for task execution by others " onclick="createObjetiveChart(kpidata.kpi233);">Number of searches for task execution by others </div>
							<div class="opcionmenu" title="Number actually executed community works for others" onclick="createObjetiveChart(kpidata.kpi234);">Number actually executed community works for others</div>
						</div>
					</div>
					<div class="useruusima">
					<div class="titSeccion">Uusima KPIs</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">MyPolls KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi240);">Number of active users</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi241);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of poll queries made by app users" onclick="createObjetiveChart(kpidata.kpi242);"> Number of poll queries made by app users</div>
							<div class="opcionmenu" title="Number of polls added to favorites list" onclick="createObjetiveChart(kpidata.kpi243);">Number of polls added to favorites list</div>
							<div class="opcionmenu" title="Number of new polls created with this app" onclick="createObjetiveChart(kpidata.kpi244);">Number of new polls created with this app</div>
							<div class="opcionmenu" title="Number of times when app user views poll statistics" onclick="createObjetiveChart(kpidata.kpi245);">Number of times when app user views poll statistics</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">MyOpinion KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi250);">Number of active users</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi251);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of times when a user searches for open polls" onclick="createObjetiveChart(kpidata.kpi252);">Number of times when a user searches for open polls</div>
							<div class="opcionmenu" title="Number of user opinions given with this app" onclick="createObjetiveChart(kpidata.kpi253);">Number of user opinions given with this app</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">MyNeighborhood KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi260);">Number of active users</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi261);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of personal preferences defined by app users" onclick="createObjetiveChart(kpidata.kpi262);">Number of personal preferences defined by app users</div>
							<div class="opcionmenu" title="Number of interesting addresses added by app users" onclick="createObjetiveChart(kpidata.kpi263);">Number of interesting addresses added by app users</div>
							<div class="opcionmenu" title="Number of neighbourhood heatmaps viewed by app users" onclick="createObjetiveChart(kpidata.kpi264);">Number of neighbourhood heatmaps viewed by app users</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">MyCityMood KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi270);">Number of active users</div>
							<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi271);">Number of times this app is launched</div>
							<div class="opcionmenu" title="Number of city mood heatmaps viewed by app users" onclick="createObjetiveChart(kpidata.kpi272);">Number of city mood heatmaps viewed by app users</div>
							<div class="opcionmenu" title="Number of city mood heatmaps added to user dashboard" onclick="createObjetiveChart(kpidata.kpi273);">Number of city mood heatmaps added to user dashboard</div>
							<div class="opcionmenu" title="Number of top problem queries made by app users" onclick="createObjetiveChart(kpidata.kpi274);">Number of top problem queries made by app users</div>
						</div>
					</div>		
					<div class="usertrento">
					<div class="titSeccion">Trento KPIs</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Street Cleaning KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi280);">Number of active users</div>
							<div class="opcionmenu" title="Number of personalized info request" onclick="createObjetiveChart(kpidata.kpi281);">Number of personalized info request</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Bike Sharing KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi290);">Number of active users</div>
							<div class="opcionmenu" title="Number of personalized info request" onclick="createObjetiveChart(kpidata.kpi291);">Number of personalized info request</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Orari Trasporti KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi300);">Number of active users</div>
							<div class="opcionmenu" title="Number of personalized info request" onclick="createObjetiveChart(kpidata.kpi301);">Number of personalized info request</div>
						</div>
					</div>									
					<div class="usernovisad">
					<div class="titSeccion">Novi Sad KPIs</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Safe City Trip KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi310);">Number of active users</div>
							<div class="opcionmenu" title="Number of personalized info request" onclick="createObjetiveChart(kpidata.kpi311);">Number of personalized info request</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Rellocation Advisor KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi320);">Number of active users</div>
							<div class="opcionmenu" title="Number of personalized info request" onclick="createObjetiveChart(kpidata.kpi321);">Number of personalized info request</div>
						</div>
						<div class="seccionMenu">
							<div class="titMenu" onclick="changeFatherOverflow(this);">Public Procurement KPIs</div>
							<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi330);">Number of active users</div>
							<div class="opcionmenu" title="Number of personalized info request" onclick="createObjetiveChart(kpidata.kpi331);">Number of personalized info request</div>
						</div>
					</div>									
				</div>

			</div>
			<div class="cuerpomenu">
				<div class="chartContainer userciudad usersuper">
					<div class="chartCabecera">
						<div class="iconCerrar" title="close all charts" onclick="document.getElementById('contenedorCharts').innerHTML='';"></div>
						<span id="titGeneral" >General data of city pilot</span>
					</div>
						<div id="contkpi0" class="chartBody">
							<div style="float:left;width:20%">
								<div class="contenedorValue">
									<div class="cabeceraValue">REGISTERED</div>
									<div id="registeredUsers" class="valorValue">0</div>
									<div class="unidadesValue">USERS</div>
								</div>
							</div>
							<div style="float:left;width:20%">
								<div class="contenedorValue">
									<div class="cabeceraValue">ACTUAL</div>
									<div id="ideasPublished" class="valorValue">0</div>
									<div class="unidadesValue">IDEAS</div>
								</div>
							</div>
							<div style="float:left;width:20%">
								<div class="contenedorValue">
									<div class="cabeceraValue">ACTUAL</div>
									<div id="challengePublished" class="valorValue">0</div>
									<div class="unidadesValue">CHALLENGES</div>
								</div>
							</div>
							<div style="float:left;width:20%">
								<div class="contenedorValue">
									<div class="cabeceraValue">ACTUAL</div>
									<div id="needNumber" class="valorValue">0</div>
									<div class="unidadesValue">NEEDS</div>
								</div>
							</div>
							<div style="float:left;width:20%">
								<div class="contenedorValue">
									<div class="cabeceraValue">ACTUAL</div>
									<div id="datasetPublished" class="valorValue">0</div>
									<div class="unidadesValue">DATASET</div>
								</div>
							</div>
						</div>
					</div>
					<div  id="contenedorCharts"></div>
			</div>
			<hr>
			</div>
	</div>
	
</body>
</html>