<%
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.model.Role"%>

<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:defineObjects />
<theme:defineObjects />
<portlet:actionURL var="selectInApp" windowState="normal" name="selectInApp">
</portlet:actionURL>
<%
	Long ccUserId = 193L;
	String pilot = "['Trento', 'Trento', 'Trento', 'Trento']";
	pilot = "['Bilbao','Bilbao', 'Bilbao', 'Bilbao']";
	String role = "ROLE-";//City Academic Bussines
	String openid="";

try{
	//expanded attributes: ("CCUserID", "pilot", "isDeveloper", "address", "city", "zipCode", "country" y "languages"):
	openid =	user.getUserUuid();
	ccUserId = (Long)user.getExpandoBridge().getAttribute("CCUserID");
    String[] pilotArray = (String [])user.getExpandoBridge().getAttribute("pilot");
    if(pilotArray != null && pilotArray.length > 0) {
        pilot = pilotArray[0];
    }
    
    if(themeDisplay.isSignedIn() && user != null) {
        List<Role> roleList = user.getRoles();
        for(Role rol : roleList) {
        	role+="-"+rol.getName();
        }
    }
    
    if(themeDisplay.getPermissionChecker().isOmniadmin()){
    	role+="-OnmiAdmin";
    }
	
	if (pilot.equals("Bilbao")){
		pilot = "['Bilbao','Bilbao', 'Bilbao', 'Bilbao']";
	}
	
	if (pilot.equals("Trento")){
		pilot = "['Trento', 'Trento', 'Trento', 'Trento']";
	}
	
	if (pilot.equals("Novisad")){
		pilot = "['Novisad','Novisad', 'Novisad', 'Novisad']";
	}
	
	if (pilot.equals("Uusimaa")){
		pilot = "['Uusimaa', 'Uusimaa', 'Uusimaa', 'Uusimaa']";
	}
	
	} catch (Exception e) {
%>
		<h1><%=e.getMessage() %></h1>
<%

	}
%>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script>
		//var urlinapp = "<%=selectInApp %>";
		//alert (urlinapp);
		var tokenInfo = {};
	    function getTokenInfo() {
	       
	
	        var url = window.location.href
	        var startIndex = url.indexOf("access_token=");
	        if (startIndex > -1) {
	          var parameters = url.substring(startIndex + "access_token=".length, url.length);
	          var endIndex = url.indexOf("&");
	
	          tokenInfo.token = url.substring(startIndex + "access_token=".length, endIndex);
	          startIndex = url.indexOf("expires_in=");
	          tokenInfo.expiration = 10000;
	
	          var seconds = parseInt(url.substring(startIndex + "expires_in=".length, url.length));
	          tokenInfo.expiration = Math.round(seconds / 60);
	        } else {
	          tokenInfo.token = "";
	        }
	
	        return tokenInfo;
	    }	
	    getTokenInfo();
	    //console.log(tokenInfo);
	    var url = window.location.href
	    var startIndex = url.indexOf("access_token=");
	    if (startIndex > -1) {
	    	var url = new URL("<%=selectInApp %>");
	    	url.searchParams.set('<portlet:namespace/>access_token', tokenInfo.token);
	    	url.searchParams.append('<portlet:namespace/>expires_in', tokenInfo.expiration);
	    	document.location=url;   	
	    
	    }
		var openid='<%=openid%>';
		console.log('openid:'+openid);
		function getInAppDatabyPilot(){
			getInAppData(pilot[0],openid);
		}
		var pilot=<%=pilot %>;
		var ccUserId=<%=ccUserId %>;
		var role="<%=role %>";
		var msgLabels={

				kpi432:'<liferay-ui:message key="ads.helsinkicitymuseums2" />',
				kpi431:'<liferay-ui:message key="ads.helsinkicitymuseums1" />',
				kpi430:'<liferay-ui:message key="ads.helsinkicitymuseums0" />',	
				
				kpi422:'<liferay-ui:message key="ads.helsinkicitybikes2" />',
				kpi421:'<liferay-ui:message key="ads.helsinkicitybikes1" />',
				kpi420:'<liferay-ui:message key="ads.helsinkicitybikes0" />',				
				
				kpi413:'<liferay-ui:message key="ads.sportit3" />',				
				kpi412:'<liferay-ui:message key="ads.sportit2" />',				
				kpi411:'<liferay-ui:message key="ads.sportit1" />',
				kpi410:'<liferay-ui:message key="ads.sportit0" />',				
				
				kpi401:'<liferay-ui:message key="ads.culturekey1" />',				
				kpi400:'<liferay-ui:message key="ads.culturekey0" />',
				
				kpi391:'<liferay-ui:message key="ads.mylocalcomunity1" />',				
				kpi390:'<liferay-ui:message key="ads.mylocalcomunity0" />',
							
				kpi383:'<liferay-ui:message key="ads.trentopaguide2" />',
				kpi382:'<liferay-ui:message key="ads.trentopaguide2" />',
				kpi381:'<liferay-ui:message key="ads.trentopaguide1" />',				
				kpi380:'<liferay-ui:message key="ads.trentopaguide0" />',

				kpi372:'<liferay-ui:message key="ads.publicspacesbooking2" />',
				kpi371:'<liferay-ui:message key="ads.publicspacesbooking1" />',				
				kpi370:'<liferay-ui:message key="ads.publicspacesbooking0" />',
								
				kpi361:'<liferay-ui:message key="ads.trentoinformer1" />',
				kpi360:'<liferay-ui:message key="ads.trentoinformer0" />',				
				
				kpi353:'<liferay-ui:message key="ads.bilbaobvents4" />',
				kpi352:'<liferay-ui:message key="ads.bilbaobvents3" />',
				kpi351:'<liferay-ui:message key="ads.bilbaobvents2" />',
				kpi350:'<liferay-ui:message key="ads.bilbaobvents1" />',				
				
				kpi344:'<liferay-ui:message key="ads.entrepreneurshipklub5" />',
				kpi343:'<liferay-ui:message key="ads.entrepreneurshipklub4" />',
				kpi342:'<liferay-ui:message key="ads.entrepreneurshipklub3" />',
				kpi341:'<liferay-ui:message key="ads.entrepreneurshipklub2" />',
				kpi340:'<liferay-ui:message key="ads.entrepreneurshipklub1" />',
						
				kpi331:'<liferay-ui:message key="ads.publicprocurementkpi2" />',
				kpi330:'<liferay-ui:message key="ads.publicprocurementkpi1" />',
				kpi321:'<liferay-ui:message key="ads.rellocationadvisorkpi2" />',
				kpi320:'<liferay-ui:message key="ads.rellocationadvisorkpi1" />',
				kpi311:'<liferay-ui:message key="ads.safecitytripkpi2" />',
				kpi310:'<liferay-ui:message key="ads.safecitytripkpi1" />',
				kpi301:'<liferay-ui:message key="ads.oraritrasportikpi2" />',
				kpi300:'<liferay-ui:message key="ads.oraritrasportikpi1" />',
				kpi291:'<liferay-ui:message key="ads.bikesharingkpi2" />',
				kpi290:'<liferay-ui:message key="ads.bikesharingkpi1" />',
				kpi281:'<liferay-ui:message key="ads.streetcleaningkpi2" />',
				kpi280:'<liferay-ui:message key="ads.streetcleaningkpi1" />',
				kpi265:'<liferay-ui:message key="ads.myneighborhoodkpi6" />',
				kpi264:'<liferay-ui:message key="ads.myneighborhoodkpi5" />',
				kpi263:'<liferay-ui:message key="ads.myneighborhoodkpi4" />',
				kpi262:'<liferay-ui:message key="ads.myneighborhoodkpi3" />',
				kpi261:'<liferay-ui:message key="ads.myneighborhoodkpi2" />',
				kpi260:'<liferay-ui:message key="ads.myneighborhoodkpi1" />',
				kpi255:'<liferay-ui:message key="ads.myopinionkpi6" />',
				kpi254:'<liferay-ui:message key="ads.myopinionkpi5" />',
				kpi253:'<liferay-ui:message key="ads.myopinionkpi4" />',
				kpi252:'<liferay-ui:message key="ads.myopinionkpi3" />',
				kpi251:'<liferay-ui:message key="ads.myopinionkpi2" />',
				kpi250:'<liferay-ui:message key="ads.myopinionkpi1" />',
				kpi246:'<liferay-ui:message key="ads.mypollskpi7" />',
				kpi245:'<liferay-ui:message key="ads.mypollskpi6" />',
				kpi244:'<liferay-ui:message key="ads.mypollskpi5" />',
				kpi243:'<liferay-ui:message key="ads.mypollskpi4" />',
				kpi242:'<liferay-ui:message key="ads.mypollskpi3" />',
				kpi241:'<liferay-ui:message key="ads.mypollskpi2" />',
				kpi240:'<liferay-ui:message key="ads.mypollskpi1" />',
				kpi224:'<liferay-ui:message key="ads.bilbonkpi5" />',
				kpi223:'<liferay-ui:message key="ads.bilbonkpi4" />',
				kpi222:'<liferay-ui:message key="ads.bilbonkpi3" />',
				kpi221:'<liferay-ui:message key="ads.bilbonkpi2" />',
				kpi220:'<liferay-ui:message key="ads.bilbonkpi1" />',
				kpi213:'<liferay-ui:message key="ads.bilbozkatukpi4" />',
				kpi212:'<liferay-ui:message key="ads.bilbozkatukpi3" />',
				kpi211:'<liferay-ui:message key="ads.bilbozkatukpi2" />',
				kpi210:'<liferay-ui:message key="ads.bilbozkatukpi1" />',
				kpi205:'<liferay-ui:message key="ads.auzonetkpi6" />',
				kpi204:'<liferay-ui:message key="ads.auzonetkpi5" />',
				kpi203:'<liferay-ui:message key="ads.auzonetkpi4" />',
				kpi202:'<liferay-ui:message key="ads.auzonetkpi3" />',
				kpi201:'<liferay-ui:message key="ads.auzonetkpi2" />',
				kpi200:'<liferay-ui:message key="ads.auzonetkpi1" />',
				kpi119:'<liferay-ui:message key="ads.appscores" />',
				kpi118:'<liferay-ui:message key="ads.recommend" />',
				kpi117:'<liferay-ui:message key="ads.happiness" />',
				kpi116:'<liferay-ui:message key="ads.usefulness" />',
				kpi115:'<liferay-ui:message key="ads.easiness" />',
				kpi114:'<liferay-ui:message key="ads.senseofcomunity" />',
				kpi113:'<liferay-ui:message key="ads.improvementsugestions" />',
				kpi112:'<liferay-ui:message key="ads.detailedfeedback" />',
				kpi111:'<liferay-ui:message key="ads.completeusersurvey" />',
				kpi109:'<liferay-ui:message key="ads.appscores" />',
				kpi108:'<liferay-ui:message key="ads.recommend" />',
				kpi107:'<liferay-ui:message key="ads.happiness" />',
				kpi106:'<liferay-ui:message key="ads.usefulness" />',
				kpi105:'<liferay-ui:message key="ads.easiness" />',
				kpi104:'<liferay-ui:message key="ads.senseofcomunity" />',
				kpi103:'<liferay-ui:message key="ads.improvementsugestions" />',
				kpi102:'<liferay-ui:message key="ads.detailedfeedback" />',
				kpi101:'<liferay-ui:message key="ads.completeusersurvey" />',
				kpi50:'<liferay-ui:message key="ads.usercontact" />',
				kpi49:'<liferay-ui:message key="ads.citiesselection" />',
				kpi48:'<liferay-ui:message key="ads.usagetools" />',
				kpi47:'<liferay-ui:message key="ads.topneeds" />',
				kpi46:'<liferay-ui:message key="ads.topdatasets" />',
				kpi45:'<liferay-ui:message key="ads.topbb" />',
				kpi44:'<liferay-ui:message key="ads.topideas" />',
				kpi43:'<liferay-ui:message key="ads.toppublicservices" />',

				kpi42dP:'<liferay-ui:message key="ads.userdeveloper" />',
				kpi42cP:'<liferay-ui:message key="ads.userrole" />',
				kpi42bP:'<liferay-ui:message key="ads.userwork" />',
				kpi42P:'<liferay-ui:message key="ads.usergender" />',
				kpi41P:'<liferay-ui:message key="ads.userage" />',
				
				
				kpi42d:'<liferay-ui:message key="ads.userdeveloper" />',
				kpi42c:'<liferay-ui:message key="ads.userrole" />',
				kpi42b:'<liferay-ui:message key="ads.userwork" />',
				kpi42:'<liferay-ui:message key="ads.usergender" />',
				kpi41:'<liferay-ui:message key="ads.userage" />',
				kpi35_1:'<liferay-ui:message key="ads.publicservicesusagecity" />',
				kpi35:'<liferay-ui:message key="ads.publicservicesusage" />',
				kpi34:'<liferay-ui:message key="ads.downloadpublicservicescity" />',
				kpi33:'<liferay-ui:message key="ads.downloadpublicservices" />',
				kpi27:'<liferay-ui:message key="ads.registeredusers" />',
				kpi20b:'<liferay-ui:message key="ads.actualbuildingbolcks" />',
				kpi20:'<liferay-ui:message key="ads.buildingbolcks" />',
				kpi18:'<liferay-ui:message key="ads.datasetusage" />',
				kpi17:'<liferay-ui:message key="ads.datasetaccesscity" />',
				kpi16:'<liferay-ui:message key="ads.topdatasets" />',
				kpi15:'<liferay-ui:message key="ads.datasetsccitytype" />',
				kpi14act:'<liferay-ui:message key="ads.actualdatasets" />',
				kpi14:'<liferay-ui:message key="ads.datasetspublished" />',
				kpi13:'<liferay-ui:message key="ads." />',//no esta
				kpi12:'<liferay-ui:message key="ads.myneeds" />',
				kpi11act:'<liferay-ui:message key="ads.actualneeds" />',
				kpi11:'<liferay-ui:message key="ads.numberneeds" />',
				kpi10:'<liferay-ui:message key="ads.mychallenges" />',
				kpi9act:'<liferay-ui:message key="ads.actualchallenges" />',
				kpi9:'<liferay-ui:message key="ads.numberchallenges" />',
				kpi8:'<liferay-ui:message key="ads.myideas" />',
				kpi7act:'<liferay-ui:message key="ads.proposedideas" />',
				kpi7:'<liferay-ui:message key="ads.proposedideas" />',
				kpi6:'<liferay-ui:message key="ads.myartefacts" />',
				kpi5act:'<liferay-ui:message key="ads.actualartefactscity" />',
				kpi4act:'<liferay-ui:message key="ads.actualartefacts" />',
				kpi5:'<liferay-ui:message key="ads.artefactspublishedcity" />',
				kpi4:'<liferay-ui:message key="ads.artefactspublished" />',
				kpi3:'<liferay-ui:message key="ads.myideasimp" />',
				kpi2:'<liferay-ui:message key="ads.ideasinimplementation" />',
				kpi1:'<liferay-ui:message key="ads.registeredusers" />',
				lbl3:'<liferay-ui:message key="ads.pilot" />',
				lbl2:'<liferay-ui:message key="ads.generaldataof" />',
				lbl1:'<liferay-ui:message key="ads.generaldatawilivepilot" />'
		}
	
		
	</script>

</head>
<body>
	<div class="fixedBodyDiv">
		<div class="cabecera">
			<div class="iconCerrarDrc actionButton" title="<liferay-ui:message key="ads.closeallchartd" />" onclick="document.getElementById('contenedorCharts').innerHTML='';currentCharts=[];"></div>
			<div class="iconActualizarDrc actionButton" title="<liferay-ui:message key="ads.refreshcharts" />" onclick="redrawCharts()"></div>
			<!-- <a href="http://in-app.cloudfoundry.welive.eu/web/" target="_blank"><div class="iconDownDrc actionButton userciudad usersuper"  ></div></a>-->
			<div class="iconDownDrc actionButton userciudad usersuper" title="<liferay-ui:message key="ads.inapp" />" onclick="document.location='<%=selectInApp %>'"></div>				 
			<div id="dates" class="datebox">
				<select style="display:none" onchange="seleccionarPerfil(this.value);">
					<option value=0>--Todos--</option>
					<option value=1>Super Usuario</option>
					<option value=2>Ciudad</option>
					<option value=3>Ciudadano</option>
				</select>
				<div class="pickerContainer" style="right:460px">	
        			<input class="pickerInput" placeholder="<liferay-ui:message key="ads.startdate" />" type="text" id="fromDate" onfocus="showPicker('fromDateMonth', this)">
        			<div id ="fromDateMonth"></div>
        		</div>
				<div class="pickerContainer" style="right:220px">	
        			<input class="pickerInput" placeholder="<liferay-ui:message key="ads.enddate" />" type="text" id="toDate" onfocus="showPicker('toDateMonth', this)">
        			<div id ="toDateMonth"></div>
        		</div>
			</div>
		</div>
		<div class="cuerpo">
			<div class="iconMenuKpis" onclick="showMenu();"><liferay-ui:message key="ads.weliveads" /></div>
			<div id="menulateral">
				<div class="fondogris"></div>
				<div class="iconCerrar cerrarMenu" onclick="closeMenu();"></div>				
				<div class="menulateral">
					<div title="Platform KPIs" class="titSeccion"><liferay-ui:message key="ads.platformkpis" /></div>
					<div class="seccionMenu" style="background-color: #bebf03;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.oia" />"><liferay-ui:message key="ads.oia" /></div>
						<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.registeredusers" />" onclick="create1DBarChart(kpidata.kpi1);">(KPI1.1) <liferay-ui:message key="ads.registeredusers" /></div>
						<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.proposedideasn" />" onclick="create1DBarChart(kpidata.kpi7);">(KPI1.2) <liferay-ui:message key="ads.proposedideas" /></div>
						
						<div class="opcionmenu userciudadano" title="<liferay-ui:message key="ads.myneeds" />" onclick="createPieChart(kpidata.kpi12);"><liferay-ui:message key="ads.myneeds" /></div>
						<div class="opcionmenu userciudadano" title="<liferay-ui:message key="ads.mychallenges" />" onclick="create1DLineChartMonth(kpidata.kpi10);"><liferay-ui:message key="ads.mychallenges" /></div>
						<div class="opcionmenu userciudadano" title="<liferay-ui:message key="ads.myideas" />" onclick="createPieChart(kpidata.kpi8);"><liferay-ui:message key="ads.myideas" /></div>	
						<div class="opcionmenu userciudadano" title="<liferay-ui:message key="ads.myideasimp" />" onclick="createPieChart(kpidata.kpi3);"><liferay-ui:message key="ads.myideasimp" /></div>					
					<!--	<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.publishedideas" />" onclick="create1DBarChart(kpidata.kpi7);">(KPI1.4) <liferay-ui:message key="ads.publishedideas" /></div>
				 		<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.actualideas" />" onclick="create1DBarChart(kpidata.kpi7act);">(KPI1.4) <liferay-ui:message key="ads.actualideas" /></div>-->
						
						<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.numberchallenges" />" onclick="create1DBarChart(kpidata.kpi9);">(KPI1.5) <liferay-ui:message key="ads.numberchallenges" /></div>
						<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.actualchallenges" />" onclick="create1DBarChart(kpidata.kpi9act);">(KPI1.5) <liferay-ui:message key="ads.actualchallenges" /></div>
						
						<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.numberneeds" />" onclick="create1DBarChart(kpidata.kpi11);">(KPI1.7) <liferay-ui:message key="ads.numberneeds" /></div>
						<div class="opcionmenu userciudad usersuper" title="<liferay-ui:message key="ads.actualneeds" />" onclick="create1DBarChart(kpidata.kpi11act);">(KPI1.7) <liferay-ui:message key="ads.actualneeds" /></div>
						
						<!-- <div class="opcionmenu" onclick="createPieChart(kpidata.kpi13);">(KPI1.7.2) Needs where I collaborate</div>-->
					</div>
					<div class="seccionMenu" style="background-color: #935a32;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.resources" />"><liferay-ui:message key="ads.resources" /></div>
						<div class="opcionmenu usersuper" title="<liferay-ui:message key="ads.artefactspublished" />" onclick="create2DBarChart(kpidata.kpi4);">(KPI1.3) <liferay-ui:message key="ads.artefactspublished" /></div>
						<div class="opcionmenu userciudad" title="<liferay-ui:message key="ads.artefactspublishedcity" />" onclick="createNumberChart(kpidata.kpi5);">(KPI1.3) <liferay-ui:message key="ads.artefactspublishedcity" /></div>
	<!--  				
						<div class="opcionmenu usersuper" title="Artefacts removed" onclick="create2DBarChart(kpidata.kpi4_1);">(KPI1.3)Artefacts removed</div>
						<div class="opcionmenu userciudad" title="Artefacts removed in City" onclick="createNumberChart(kpidata.kpi5_1);">(KPI1.3) Artefacts removed in the City</div>
	-->
						<div class="opcionmenu usersuper" title="<liferay-ui:message key="ads.actualartefacts" />" onclick="create2DBarChart(kpidata.kpi4act);">(KPI1.3)<liferay-ui:message key="ads.actualartefacts" /></div>
						<div class="opcionmenu userciudad" title="<liferay-ui:message key="ads.actualartefactscity" />" onclick="createNumberChart(kpidata.kpi5act);">(KPI1.3) <liferay-ui:message key="ads.actualartefactscity" /></div>
						<div class="opcionmenu userciudadano" title="<liferay-ui:message key="ads.myartefacts" />" onclick="create2DBarChart(kpidata.kpi6);"><liferay-ui:message key="ads.myartefacts" /></div>
						
					</div>
					<div class="seccionMenu" style="background-color: #e44240;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.opendatastack" />"><liferay-ui:message key="ads.opendatastack" /></div>
						<div class="opcionmenu  userciudad usersuper" title="<liferay-ui:message key="ads.datasetspublished" />" onclick="create1DBarChart(kpidata.kpi14);">(KPI2.1) <liferay-ui:message key="ads.datasetspublished" /></div>
						<div class="opcionmenu  userciudad usersuper" title="<liferay-ui:message key="ads.actualdatasets" />" onclick="create1DBarChart(kpidata.kpi14act);">(KPI2.1) <liferay-ui:message key="ads.actualdatasets" /></div>
						<div class="opcionmenu  userciudad usersuper" title="<liferay-ui:message key="ads.datasetsccitytype" />" onclick="create2DBarChart(kpidata.kpi15);">(KPI2.2) <liferay-ui:message key="ads.datasetsccitytype" /></div>
						<div class="opcionmenu  usersuper" title="<liferay-ui:message key="ads.topdatasets" />" onclick="create1DBarChartTop(kpidata.kpi16, 10);">(KPI2.3) <liferay-ui:message key="ads.topdatasets" /></div>
						<div class="opcionmenu  userciudad" title="<liferay-ui:message key="ads.datasetaccesscity" />" onclick="create1DBarChartTop(kpidata.kpi17, 10);">(KPI2.3) <liferay-ui:message key="ads.datasetaccesscity" /></div>
						<div class="opcionmenu userciudadano" title="<liferay-ui:message key="ads.datasetusage" />" onclick="createPieChart(kpidata.kpi18);"><liferay-ui:message key="ads.datasetusage" /></div>
					</div>
					<div class="seccionMenu userciudad usersuper" style="background-color: #6eb0df;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.openserviceframework" />"><liferay-ui:message key="ads.openserviceframework" /></div>
						<div class="opcionmenu" title="<liferay-ui:message key="ads.buildingbolcks" />" onclick="create1DBarChart(kpidata.kpi20);">(KPI3.1) <liferay-ui:message key="ads.buildingbolcks" /></div>
						<div class="opcionmenu" title="<liferay-ui:message key="ads.actualbuildingbolcks" />" onclick="create1DBarChart(kpidata.kpi20b);">(KPI3.1) <liferay-ui:message key="ads.actualbuildingbolcks" /></div>
					</div>
					<div class="seccionMenu userciudad usersuper" style="background-color: #5064ef;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.personalizationpublicservices" />"><liferay-ui:message key="ads.personalizationpublicservices" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.registeredusers" />"  onclick="create1DBarChart(kpidata.kpi27);">(KPI4.3) <liferay-ui:message key="ads.registeredusers" /></div>
					</div>
					<div class="seccionMenu userciudad usersuper"  style="background-color: #8d6e63;" >
						<div class="titMenu"onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.stakeholderehaviourchange" />"><liferay-ui:message key="ads.stakeholderehaviourchange" /></div>
					<div class="opcionmenu  usersuper" title="<liferay-ui:message key="ads.downloadpublicservices" />" onclick="create1DBarChart(kpidata.kpi33);">(KPI5.3) <liferay-ui:message key="ads.downloadpublicservices" /></div>
					<div class="opcionmenu  userciudad" title="<liferay-ui:message key="ads.downloadpublicservicescity" />" onclick="create1DBarChart(kpidata.kpi34);">(KPI5.3) <liferay-ui:message key="ads.downloadpublicservicescity" /></div>
					<div class="opcionmenu usersuper" title="<liferay-ui:message key="ads.publicservicesusage" />" onclick="create1DBarChart(kpidata.kpi35);">(KPI5.4)<liferay-ui:message key="ads.publicservicesusage" /></div>
					<div class="opcionmenu  userciudad" title="<liferay-ui:message key="ads.publicservicesusagecity" />" onclick="create1DBarChart(kpidata.kpi35_1);">(KPI5.4) <liferay-ui:message key="ads.publicservicesusagecity" /></div>
					</div>
					<div class="seccionMenu  userciudad usersuper" style="background-color: #ab47bc;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.demograficstatistics" />"><liferay-ui:message key="ads.demograficstatistics" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.userage" />"  onclick="create2DBarChartOld(kpidata.kpi41);">(KPI11.1) <liferay-ui:message key="ads.userage" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.usergender" />"  onclick="create2DBarChartOld(kpidata.kpi42);">(KPI11.2) <liferay-ui:message key="ads.usergender" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.userwork" />"  onclick="create2DBarChartOld(kpidata.kpi42b);">(KPI11.3) <liferay-ui:message key="ads.userwork" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.userrole" />"  onclick="create2DBarChartOld(kpidata.kpi42c);">(KPI11.4) <liferay-ui:message key="ads.userrole" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.userdeveloper" />"  onclick="create2DBarChartOld(kpidata.kpi42d);">(KPI11.5) <liferay-ui:message key="ads.userdeveloper" /></div>
					<div class="opcionmenu" title="%<liferay-ui:message key="ads.userage" />"  onclick="create2DBarChartOldPercentage(kpidata.kpi41P);">(KPI11.1B)% <liferay-ui:message key="ads.userage" /></div>
					<div class="opcionmenu" title="%<liferay-ui:message key="ads.usergender" />"  onclick="create2DBarChartOldPercentage(kpidata.kpi42P);">(KPI11.2B)% <liferay-ui:message key="ads.usergender" /></div>
					<div class="opcionmenu" title="%<liferay-ui:message key="ads.userwork" />"  onclick="create2DBarChartOldPercentage(kpidata.kpi42bP);">(KPI11.3B)% <liferay-ui:message key="ads.userwork" /></div>
					<div class="opcionmenu" title="%<liferay-ui:message key="ads.userrole" />"  onclick="create2DBarChartOldPercentage(kpidata.kpi42cP);">(KPI11.4B)% <liferay-ui:message key="ads.userrole" /></div>
					<div class="opcionmenu" title="%<liferay-ui:message key="ads.userdeveloper" />"  onclick="create2DBarChartOldPercentage(kpidata.kpi42dP);">(KPI11.5B)% <liferay-ui:message key="ads.userdeveloper" /></div>
					</div>
					<div class="seccionMenu userciudad usersuper" style="background-color: #ffa726;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.rankings" />"><liferay-ui:message key="ads.rankings" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.toppublicservices" />" onclick="create1DBarChartTop(kpidata.kpi43);">(KPI12.1) <liferay-ui:message key="ads.toppublicservices" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.topideas" />" onclick="create1DBarChartTop(kpidata.kpi44);">(KPI12.2) <liferay-ui:message key="ads.topideas" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.topbb" />" onclick="create1DBarChartTop(kpidata.kpi45);">(KPI12.3) <liferay-ui:message key="ads.topbb" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.topdatasets" />" onclick="create1DBarChartTop(kpidata.kpi46);">(KPI12.4) <liferay-ui:message key="ads.topdatasets" /></div>
					<div class="opcionmenu" title="<liferay-ui:message key="ads.topneeds" />" onclick="create1DBarChartTop(kpidata.kpi47);">(KPI12.6) <liferay-ui:message key="ads.topneeds" /></div>
					</div>
					<div class="seccionMenu  userciudad usersuper" style="background-color: #78909c;" >
						<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.weliveframework" />"><liferay-ui:message key="ads.weliveframework" /></div>
					<div class="opcionmenu  userciudad usersuper" title="<liferay-ui:message key="ads.usagetools" />" onclick="create1DBarChart(kpidata.kpi48);">(KPI13.1) <liferay-ui:message key="ads.usagetools" /></div>
					<div class="opcionmenu  userciudad usersuper" title="<liferay-ui:message key="ads.citiesselection" />" onclick="create1DBarChart(kpidata.kpi49);">(KPI13.2) <liferay-ui:message key="ads.citiesselection" /></div>
					<div class="opcionmenu  usersuper" title="<liferay-ui:message key="ads.usercontact" />" onclick="create1DLineChartMonth(kpidata.kpi50);">(KPI13.3) <liferay-ui:message key="ads.usercontact" /></div>
					</div>
	
					<div class="usersuper userciudad">
						<div class="titSeccion" title="<liferay-ui:message key="ads.applicationkpis" />"><liferay-ui:message key="ads.applicationkpis" /></div>
						<div class="seccionMenu" style="background-color: #ef5350;" >
							<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.commonapps" />"><liferay-ui:message key="ads.commonapps" /></div>
							<div class="usersuper">
								<div class="opcionmenu" title="<liferay-ui:message key="ads.completeusersurvey" />" onclick="create1DBarChart(kpidata.kpi101);">(KPI1.A) <liferay-ui:message key="ads.completeusersurvey" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.detailedfeedback" />" onclick="create1DBarChart(kpidata.kpi102);">(KPI1.B) <liferay-ui:message key="ads.detailedfeedback" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.improvementsugestions" />" onclick="create1DBarChart(kpidata.kpi103);">(KPI2 ) <liferay-ui:message key="ads.improvementsugestions" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.senseofcomunity" />" onclick="create1DBarChart(kpidata.kpi104);">(KPI3) <liferay-ui:message key="ads.senseofcomunity" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.easiness" />" onclick="createAVGBarChart(kpidata.kpi105);">(KPI4A) <liferay-ui:message key="ads.easiness" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.usefulness" />" onclick="createAVGBarChart(kpidata.kpi106);">(KPI4B) <liferay-ui:message key="ads.usefulness" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.happiness" />" onclick="createAVGBarChart(kpidata.kpi107);">(KPI4C) <liferay-ui:message key="ads.happiness" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.recommend" />" onclick="createAVGBarChart(kpidata.kpi108);">(KPI4D) <liferay-ui:message key="ads.recommend" /></div>
								<!--  
								<div class="opcionmenu" title="<liferay-ui:message key="ads.appscores" />" onclick="create1DBarChart(kpidata.kpi109);">(KPI5) <liferay-ui:message key="ads.appscores" /></div>
								-->
							</div>
							<div class="userciudad">
								<div class="opcionmenu" title="<liferay-ui:message key="ads.completeusersurvey" />" onclick="create1DBarChart(kpidata.kpi111);">(KPI1.A Pilot) <liferay-ui:message key="ads.completeusersurvey" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.detailedfeedback" />" onclick="create1DBarChart(kpidata.kpi112);">(KPI1.B Pilot) <liferay-ui:message key="ads.detailedfeedback" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.improvementsugestions" />" onclick="create1DBarChart(kpidata.kpi113);">(KPI2 Pilot) <liferay-ui:message key="ads.improvementsugestions" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.senseofcomunity" />" onclick="create1DBarChart(kpidata.kpi114);">(KPI3 Pilot) <liferay-ui:message key="ads.senseofcomunity" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.easiness" />" onclick="createAVGBarChart(kpidata.kpi115);">(KPI4A) <liferay-ui:message key="ads.easiness" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.usefulness" />" onclick="createAVGBarChart(kpidata.kpi116);">(KPI4B) <liferay-ui:message key="ads.usefulness" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.happiness" />" onclick="createAVGBarChart(kpidata.kpi117);">(KPI4C) <liferay-ui:message key="ads.happiness" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.recommend" />" onclick="createAVGBarChart(kpidata.kpi118);">(KPI4D) <liferay-ui:message key="ads.recommend" /></div>
								<!--  
								<div class="opcionmenu" title="<liferay-ui:message key="ads.appscores" />" onclick="create1DBarChart(kpidata.kpi119);">(KPI5 Pilot)  <liferay-ui:message key="ads.appscores" /></div>
								-->
							</div>
						</div>
						<div class="userbilbao">
						<div class="titSeccion" title="<liferay-ui:message key="ads.bilbaokpis" />"><liferay-ui:message key="ads.bilbaokpis" /></div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.bilbozkatukpis" />"><liferay-ui:message key="ads.bilbozkatukpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbozkatukpi1" />" onclick="createObjetiveChart(kpidata.kpi210);"><liferay-ui:message key="ads.bilbozkatukpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbozkatukpi2" />" onclick="createObjetiveChart(kpidata.kpi211);"><liferay-ui:message key="ads.bilbozkatukpi2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbozkatukpi3" />" onclick="createObjetiveChart(kpidata.kpi212);"><liferay-ui:message key="ads.bilbozkatukpi3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbozkatukpi4" />" onclick="createObjetiveChart(kpidata.kpi213);"><liferay-ui:message key="ads.bilbozkatukpi4" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.bilbonkpis" />"><liferay-ui:message key="ads.bilbonkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbonkpi1" />" onclick="createObjetiveChart(kpidata.kpi220);"><liferay-ui:message key="ads.bilbonkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbonkpi2" />" onclick="createObjetiveChart(kpidata.kpi221);"><liferay-ui:message key="ads.bilbonkpi2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbonkpi3" />" onclick="createObjetiveChart(kpidata.kpi222);"><liferay-ui:message key="ads.bilbonkpi3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbonkpi4" />" onclick="createObjetiveChart(kpidata.kpi223);"><liferay-ui:message key="ads.bilbonkpi4" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbonkpi5" />" onclick="createObjetiveChart(kpidata.kpi224);"><liferay-ui:message key="ads.bilbonkpi5" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.auzonetkpis" />"><liferay-ui:message key="ads.auzonetkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.auzonetkpi1" />" onclick="createObjetiveChart(kpidata.kpi200);"><liferay-ui:message key="ads.auzonetkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.auzonetkpi2" />" onclick="createObjetiveChart(kpidata.kpi201);"><liferay-ui:message key="ads.auzonetkpi2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.auzonetkpi3" />" onclick="createObjetiveChart(kpidata.kpi202);"><liferay-ui:message key="ads.auzonetkpi3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.auzonetkpi4" />" onclick="createObjetiveChart(kpidata.kpi203);"><liferay-ui:message key="ads.auzonetkpi4" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.auzonetkpi5" />" onclick="createObjetiveChart(kpidata.kpi204);"><liferay-ui:message key="ads.auzonetkpi5" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.auzonetkpi6" />" onclick="createObjetiveChart(kpidata.kpi205);"><liferay-ui:message key="ads.auzonetkpi6" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.entrepreneurshipklub" />"><liferay-ui:message key="ads.entrepreneurshipklub" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.entrepreneurshipklub1" />" onclick="createObjetiveChart(kpidata.kpi340);"><liferay-ui:message key="ads.entrepreneurshipklub1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.entrepreneurshipklub2" />" onclick="createObjetiveChart(kpidata.kpi341);"><liferay-ui:message key="ads.entrepreneurshipklub2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.entrepreneurshipklub3" />" onclick="createObjetiveChart(kpidata.kpi342);"><liferay-ui:message key="ads.entrepreneurshipklub3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.entrepreneurshipklub4" />" onclick="createObjetiveChart(kpidata.kpi343);"><liferay-ui:message key="ads.entrepreneurshipklub4" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.entrepreneurshipklub5" />" onclick="createObjetiveChart(kpidata.kpi344);"><liferay-ui:message key="ads.entrepreneurshipklub5" /></div>
								
							</div>		
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.bilbaobvents" />"><liferay-ui:message key="ads.bilbaobvents" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbaobvents1" />" onclick="createObjetiveChart(kpidata.kpi350);"><liferay-ui:message key="ads.bilbaobvents1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbaobvents2" />" onclick="createObjetiveChart(kpidata.kpi351);"><liferay-ui:message key="ads.bilbaobvents2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbaobvents3" />" onclick="createObjetiveChart(kpidata.kpi352);"><liferay-ui:message key="ads.bilbaobvents3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bilbaobvents4" />" onclick="createObjetiveChart(kpidata.kpi353);"><liferay-ui:message key="ads.bilbaobvents4" /></div>
							</div>							
	
							<!--  
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);">Faborenet KPIs</div>
								<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi230);">Number of times this app is launched</div>
								<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi231);">Number of active users</div>
								<div class="opcionmenu" title="Number of user profile skills and service offers registered" onclick="createObjetiveChart(kpidata.kpi232);">Number of user profile skills and service offers registered</div>
								<div class="opcionmenu" title="Number of searches for task execution by others " onclick="createObjetiveChart(kpidata.kpi233);">Number of searches for task execution by others</div>
								<div class="opcionmenu" title="Number actually executed community works for others" onclick="createObjetiveChart(kpidata.kpi234);">Number actually executed community works for others</div>
							</div>
							-->
						</div>
						<div class="useruusima">
						<div class="titSeccion" title="<liferay-ui:message key="ads.uusimaakpis" />"><liferay-ui:message key="ads.uusimaakpis" /></div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.mypollskpis" />"><liferay-ui:message key="ads.mypollskpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi1" />" onclick="createObjetiveChart(kpidata.kpi240);"><liferay-ui:message key="ads.mypollskpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi2" />" onclick="createObjetiveChart(kpidata.kpi241);"><liferay-ui:message key="ads.mypollskpi2" /></div>
							<!--  
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi3" />" onclick="createObjetiveChart(kpidata.kpi242);"><liferay-ui:message key="ads.mypollskpi3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi4" />" onclick="createObjetiveChart(kpidata.kpi243);"><liferay-ui:message key="ads.mypollskpi4" /></div>
							-->
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi5" />" onclick="createObjetiveChart(kpidata.kpi244);"><liferay-ui:message key="ads.mypollskpi5" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi6" />" onclick="createObjetiveChart(kpidata.kpi245);"><liferay-ui:message key="ads.mypollskpi6" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mypollskpi7" />" onclick="createObjetiveChart(kpidata.kpi246);"><liferay-ui:message key="ads.mypollskpi7" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.myopinionkpis" />"><liferay-ui:message key="ads.myopinionkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myopinionkpi1" />" onclick="createObjetiveChart(kpidata.kpi250);"><liferay-ui:message key="ads.myopinionkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myopinionkpi2" />" onclick="createObjetiveChart(kpidata.kpi251);"><liferay-ui:message key="ads.myopinionkpi2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myopinionkpi3" />" onclick="createObjetiveChart(kpidata.kpi252);"><liferay-ui:message key="ads.myopinionkpi3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myopinionkpi4" />" onclick="createObjetiveChart(kpidata.kpi253);"><liferay-ui:message key="ads.myopinionkpi4" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myopinionkpi5" />" onclick="createObjetiveChart(kpidata.kpi254);"><liferay-ui:message key="ads.myopinionkpi4" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myopinionkpi6" />" onclick="createObjetiveChart(kpidata.kpi255);"><liferay-ui:message key="ads.myopinionkpi5" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.myneighborhoodkpis" />"><liferay-ui:message key="ads.myneighborhoodkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myneighborhoodkpi1" />" onclick="createObjetiveChart(kpidata.kpi260);"><liferay-ui:message key="ads.myneighborhoodkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myneighborhoodkpi2" />" onclick="createObjetiveChart(kpidata.kpi261);"><liferay-ui:message key="ads.myneighborhoodkpi2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myneighborhoodkpi3" />" onclick="createObjetiveChart(kpidata.kpi262);"><liferay-ui:message key="ads.myneighborhoodkpi3" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myneighborhoodkpi4" />" onclick="createObjetiveChart(kpidata.kpi263);"><liferay-ui:message key="ads.myneighborhoodkpi4" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myneighborhoodkpi5" />" onclick="createObjetiveChart(kpidata.kpi264);"><liferay-ui:message key="ads.myneighborhoodkpi5" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.myneighborhoodkpi6" />" onclick="createObjetiveChart(kpidata.kpi265);"><liferay-ui:message key="ads.myneighborhoodkpi6" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="sportit" />"><liferay-ui:message key="ads.sportit" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.sportit3" />" onclick="createObjetiveChart(kpidata.kpi413);"><liferay-ui:message key="ads.sportit3" /></div>						
								<div class="opcionmenu" title="<liferay-ui:message key="ads.sportit0" />" onclick="createObjetiveChart(kpidata.kpi410);"><liferay-ui:message key="ads.sportit0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.sportit1" />" onclick="createObjetiveChart(kpidata.kpi411);"><liferay-ui:message key="ads.sportit1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.sportit2" />" onclick="createObjetiveChart(kpidata.kpi412);"><liferay-ui:message key="ads.sportit2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="helsinkicitybikes" />"><liferay-ui:message key="ads.helsinkicitybikes" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.helsinkicitybikes2" />" onclick="createObjetiveChart(kpidata.kpi422);"><liferay-ui:message key="ads.helsinkicitybikes2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.helsinkicitybikes0" />" onclick="createObjetiveChart(kpidata.kpi420);"><liferay-ui:message key="ads.helsinkicitybikes0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.helsinkicitybikes1" />" onclick="createObjetiveChart(kpidata.kpi421);"><liferay-ui:message key="ads.helsinkicitybikes1" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="helsinkicitymuseums" />"><liferay-ui:message key="ads.helsinkicitymuseums" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.helsinkicitymuseums2" />" onclick="createObjetiveChart(kpidata.kpi432);"><liferay-ui:message key="ads.helsinkicitymuseums2" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.helsinkicitymuseums0" />" onclick="createObjetiveChart(kpidata.kpi430);"><liferay-ui:message key="ads.helsinkicitymuseums0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.helsinkicitymuseums1" />" onclick="createObjetiveChart(kpidata.kpi431);"><liferay-ui:message key="ads.helsinkicitymuseums1" /></div>
							</div>
									
							<!--  
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);">MyCityMood KPIs</div>
								<div class="opcionmenu" title="Number of active users" onclick="createObjetiveChart(kpidata.kpi270);">Number of active users</div>
								<div class="opcionmenu" title="Number of times this app is launched" onclick="createObjetiveChart(kpidata.kpi271);">Number of times this app is launched</div>
								<div class="opcionmenu" title="Number of city mood heatmaps viewed by app users" onclick="createObjetiveChart(kpidata.kpi272);">Number of city mood heatmaps viewed by app users</div>
								<div class="opcionmenu" title="Number of city mood heatmaps added to user dashboard" onclick="createObjetiveChart(kpidata.kpi273);">Number of city mood heatmaps added to user dashboard</div>
								<div class="opcionmenu" title="Number of top problem queries made by app users" onclick="createObjetiveChart(kpidata.kpi274);">Number of top problem queries made by app users</div>
							</div>
							-->
						</div>		
						<div class="usertrento">
						<div class="titSeccion" title="<liferay-ui:message key="ads.trentokpis" />"><liferay-ui:message key="ads.trentokpis" /></div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.streetcleaningkpis" />"><liferay-ui:message key="ads.streetcleaningkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.streetcleaningkpi1" />" onclick="createObjetiveChart(kpidata.kpi280);"><liferay-ui:message key="ads.streetcleaningkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.streetcleaningkpi2" />" onclick="createObjetiveChart(kpidata.kpi281);"><liferay-ui:message key="ads.streetcleaningkpi2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.bikesharingkpis" />"><liferay-ui:message key="ads.bikesharingkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bikesharingkpi1" />" onclick="createObjetiveChart(kpidata.kpi290);"><liferay-ui:message key="ads.bikesharingkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.bikesharingkpi2" />" onclick="createObjetiveChart(kpidata.kpi291);"><liferay-ui:message key="ads.bikesharingkpi2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.oraritrasportikpis" />Orari Trasporti KPIs"><liferay-ui:message key="ads.oraritrasportikpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.oraritrasportikpi1" />" onclick="createObjetiveChart(kpidata.kpi300);"><liferay-ui:message key="ads.oraritrasportikpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.oraritrasportikpi2" />" onclick="createObjetiveChart(kpidata.kpi301);"><liferay-ui:message key="ads.oraritrasportikpi2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.trentoinformer" />"><liferay-ui:message key="ads.trentoinformer" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.oraritrasportikpi1" />" onclick="createObjetiveChart(kpidata.kpi360);"><liferay-ui:message key="ads.trentoinformer0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.oraritrasportikpi2" />" onclick="createObjetiveChart(kpidata.kpi361);"><liferay-ui:message key="ads.trentoinformer1" /></div>
							</div>		
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.publicspacesbooking" />"><liferay-ui:message key="ads.publicspacesbooking" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.publicspacesbooking0" />" onclick="createObjetiveChart(kpidata.kpi370);"><liferay-ui:message key="ads.publicspacesbooking0" /></div>
							</div>		
							<!-- <div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.trentopaguide" />"><liferay-ui:message key="ads.trentopaguide" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.trentopaguide0" />" onclick="createObjetiveChart(kpidata.kpi380);"><liferay-ui:message key="ads.trentopaguide0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.trentopaguide1" />" onclick="createObjetiveChart(kpidata.kpi381);"><liferay-ui:message key="ads.trentopaguide1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.trentopaguide2" />" onclick="createObjetiveChart(kpidata.kpi382);"><liferay-ui:message key="ads.trentopaguide2" /></div>
							</div>-->		
								
						</div>									
						<div class="usernovisad">
						<div class="titSeccion" title="<liferay-ui:message key="ads.novisadkpis" />"><liferay-ui:message key="ads.novisadkpis" /></div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.safecitytripkpis" />"><liferay-ui:message key="ads.safecitytripkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.safecitytripkpi1" />" onclick="createObjetiveChart(kpidata.kpi310);"><liferay-ui:message key="ads.safecitytripkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.safecitytripkpi2" />" onclick="createObjetiveChart(kpidata.kpi311);"><liferay-ui:message key="ads.safecitytripkpi2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.rellocationadvisorkpis" />"><liferay-ui:message key="ads.rellocationadvisorkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.rellocationadvisorkpi1" />" onclick="createObjetiveChart(kpidata.kpi320);"><liferay-ui:message key="ads.rellocationadvisorkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.rellocationadvisorkpi2" />" onclick="createObjetiveChart(kpidata.kpi321);"><liferay-ui:message key="ads.rellocationadvisorkpi2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.publicprocurementkpis" />"><liferay-ui:message key="ads.publicprocurementkpis" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.publicprocurementkpi1" />" onclick="createObjetiveChart(kpidata.kpi330);"><liferay-ui:message key="ads.publicprocurementkpi1" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.publicprocurementkpi2" />" onclick="createObjetiveChart(kpidata.kpi331);"><liferay-ui:message key="ads.publicprocurementkpi2" /></div>
							</div>
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.mylocalcomunity" />"><liferay-ui:message key="ads.mylocalcomunity" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mylocalcomunity0" />" onclick="createObjetiveChart(kpidata.kpi390);"><liferay-ui:message key="ads.mylocalcomunity0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.mylocalcomunity1" />" onclick="createObjetiveChart(kpidata.kpi391);"><liferay-ui:message key="ads.mylocalcomunity1" /></div>
							</div>							
							
							<div class="seccionMenu">
								<div class="titMenu" onclick="changeFatherOverflow(this);" title="<liferay-ui:message key="ads.culturekey" />"><liferay-ui:message key="ads.culturekey" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.culturekey0" />" onclick="createObjetiveChart(kpidata.kpi400);"><liferay-ui:message key="ads.culturekey0" /></div>
								<div class="opcionmenu" title="<liferay-ui:message key="ads.culturekey1" />" onclick="createObjetiveChart(kpidata.kpi401);"><liferay-ui:message key="ads.culturekey1" /></div>
							</div>							
						</div>									
					</div>
				</div>
			</div>
			<div class="cuerpomenu">
				<div class="chartContainer userciudad usersuper">
					<div class="chartCabecera">
						<span id="titGeneral" >General data</span>
					</div>
						<div id="contkpi0" class="chartBodyCabecera">
							<div class="contenedorAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.users"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.users" /></div>
									<div id="registeredUsers" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
							<div class="contenedorAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.ideas"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.ideas" /></div>
									<div id="ideasPublished" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
							<div class="contenedorAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.challenges"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.challenges" /></div>
									<div id="challengePublished" class="valorValue">0</div>
									<div class="cabeceraValue"></div>									
								</div>
							</div>
							<div class="contenedorAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.needs"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.needs" /></div>
									<div id="needNumber" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
							<div class="contenedorAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.datasets"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.datasets" /></div>
									<div id="datasetPublished" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
						</div>
					</div>
				<div class="chartContainer userciudadano">
					<div class="chartCabecera">
						<span id="titGeneral" ><liferay-ui:message key="ads.titleCitizen" /></span>
					</div>
						<div id="contkpi0" class="chartBodyCabecera">
							<div class="contenedorNoAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.myneeds"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.myneeds" /></div>
									<div id="userNeeds" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
							<div class="contenedorNoAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.mychallenges"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.mychallenges" /></div>
									<div id="userChallenges" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
									
								</div>
							</div>
							<div class="contenedorNoAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.myideas"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.myideas" /></div>
									<div id="userIdeas" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
									
								</div>
							</div>						
							<div class="contenedorNoAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.myideasimp"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.myideasimp" /></div>
									<div id="userIdeasImpl" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
							<div class="contenedorNoAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.myartefacts"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.myartefacts" /></div>
									<div id="userArtefacts" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>
							<div class="contenedorNoAdmin tooltipped" data-position="bottom" data-delay="50" data-tooltip='<liferay-ui:message key="ads.datasetusage"/>'>
								<div class="contenedorValue">
									<div class="unidadesValue"><liferay-ui:message key="ads.datasetusage" /></div>									
									<div id="userDatasets" class="valorValue">0</div>
									<div class="cabeceraValue"></div>
								</div>
							</div>		
						</div>
					</div>

					
					<div  class="contenedor" id="contenedorCharts"></div>
			</div>
			<hr>
			</div>
	</div>
	
</body>
</html>