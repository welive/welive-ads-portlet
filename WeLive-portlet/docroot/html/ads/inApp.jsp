<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="eu.welive.ads.util.PropertyGetter"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<portlet:defineObjects />
<theme:defineObjects />
<%
PropertyGetter propertyGetter = PropertyGetter.getInstance();
String base_url ="";
try{
	base_url = propertyGetter.getProperty("welive.ads.url");
}
catch(Exception e){
	base_url = "https://dev.welive.eu";
}

if (base_url == null || base_url.equals(""))
	base_url = "https://dev.welive.eu";
%>
<!DOCTYPE html>
<html>
<head>
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
    
</head>

<body>
  
<div id="materialize-body" class="materialize">
  <h1>In-App Questionnaire Responses</h1>

  <p>The page downloads a zip file containing a CSV file with the response data
  for the selected pilot.</p>

  <p><b>Important Note:</b> The pilot id is stored as supplied by the
    applications (it should be one of following: <i>Bilbao</i>, <i>Trento</i>,
    <i>Novisad</i>, <i>Uusimaa</i>). Therefore, if the id supplied by the your
    apps is not correct, the response will be not correctly retrieved if you
    select your specific pilot.
  </p>
<div class="row">
  <form class="col s12">
  <div class="row">
        <div class="col s3">
	    
	     Pilot:
	     <div class="input-field inline">
	    <select id="pilotId">
	      <option value="All">All</option>
	      <option value="Bilbao">Bilbao</option>
	      <option value="Trento">Trento</option>
	      <option value="Novisad">Novisad</option>
	      <option value="Uusimaa">Uusimaa</option>
	    </select>
	   </div>
	        </div>
    </div>
    <input id="submitButton" type="button" value="Download data">
  </form>
</div>
  <br/>
  
  <span id="message"></span>
   <br/>
  <span id="statusMessage"></span>
  <br/>
	<a id="loginButton" class="btn light-blue lighten-1" href="#" onclick="buttonPressed()">Login</a>
	</div>
  <script>
  	var base_url='<%=base_url%>';
  	
    $(function () {
      window.clientID = "76efe817-4307-4fb0-a9dc-39eb9327c514";//"a2023c99-d83a-4fea-b768-253219439839";

      
     window.redirectURL = base_url+'/ads'; 
    	  //document.URL.substring(0, index) + 'ads/inApp.jsp';
      
      window.tokenInfo = getTokenInfo();
      if (tokenInfo.token.length > 0) {
        window.expiration = window.tokenInfo.expiration;
        $("#loginButton").html('Back')
        $("#submitButton").show();
        $("#loginButton").toggleClass('red lighten-1 white-text btn btn-flat')
        setTimeout(decreaseExpiration, 60000);
        getBasicInfo();
      } else {
        $("#loginButton").html('Login');
        $("#submitButton").hide();
        buttonPressed();
      }
    });

    function getBasicInfo() {
      $.ajax({
           url: base_url+'/dev/api/aac/basicprofile/me',
           type: "GET",
           headers: {'Authorization': 'Bearer ' + tokenInfo.token},
           success: function(user_data) {
             window.user_data = user_data;
             updateMessage();
           },
           error: function(data) {
             user_data = {
                "name": "Name",
                "surname": "Surname",
                "socialId": "0",
                "userId": "0"
             };
             window.user_data = user_data;
             $("#message").html('User validation not working');
           }
        });
    }

    function buttonPressed() {
      if ($("#loginButton").html() == 'Login') {
        var auth_url = base_url+"/aac/eauth/authorize?client_id=" + window.clientID + "&response_type=token&redirect_uri=" + window.redirectURL + "&scope=profile.basicprofile.me,profile.accountprofile.me";
        location.href = auth_url;
      } else {
        logout();
      }
    }

    function logout() {
      var logout_url = base_url+"/ads";
      location.href = logout_url;
    }

    function getTokenInfo() {
      var tokenInfo = {}

      var url = new URL(window.location.href);
      tokenInfo.token = url.searchParams.get("<portlet:namespace/>access_token");
      if (tokenInfo.token === null) tokenInfo.token ="";
      var seconds =  url.searchParams.get("<portlet:namespace/>expires_in");
      tokenInfo.expiration = Math.round(seconds / 60);
      
	  console.log(tokenInfo);
	  
      return tokenInfo;
    }

    function decreaseExpiration() {
      window.expiration = window.expiration - 1;
      if (window.expiration > 0) {
          updateMessage();
          setTimeout(decreaseExpiration, 60000);
      } else {
          $("#message").html("Token expired");
          $("#loginButton").html('Login');
          $("#loginButton").toggleClass('red lighten-1 white-text btn btn-flat')
          $("#submitButton").hide();
          
      }
    }

    function updateMessage() {
      var user_data = window.user_data;
      var message = 'You are logged in';
      $("#message").html(message);
    }

    $('#submitButton').on('click',function() {
      $("#statusMessage").html('Downloading data. Please wait ...');
      $.ajax({//http://in-app.cloudfoundry.welive.eu/api/questionnaire/get-responses?pilotId=All
        url: "https://in-app.cloudfoundry.welive.eu/api/questionnaire/get-responses",
        type: 'GET',
        data: {'pilotId': $( "#pilotId option:selected" ).val() },
        dataType: 'text',
        mimeType: 'text/plain; charset=x-user-defined',
        headers: {
          "Authorization": 'Bearer ' + tokenInfo.token
        },
        success: function(data) {
          download_file("responses.zip", data, "application/zip");
        },
        error: function(data) {
          var errorMsg = JSON.parse(data.responseText);
          $("#statusMessage").html('<b>' + errorMsg.message + '</b></br><a href="http://in-app.cloudfoundry.welive.eu/web/" class="btn light-blue lighten-1" target="_blank">Try here,...</a></br>');
        }
      })});

    function process_bytes(data) {
      newContent = "";

      for (var i = 0; i < data.length; i++) {
        newContent += String.fromCharCode(data.charCodeAt(i) & 0xFF);
      }

      var bytes = new Uint8Array(newContent.length);
      for (var i = 0; i < newContent.length; i++) {
        bytes[i] = newContent.charCodeAt(i);
      }

      return bytes;
    }

    function download_file(name, contents, mime_type) {
      var bytes = process_bytes(contents);
      var blob = new Blob([bytes], {type: mime_type});
      saveAs(blob, "response.zip");
    }
    
  </script>
</body>
</html>
